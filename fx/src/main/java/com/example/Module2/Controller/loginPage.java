
package com.example.Module2.Controller;

import java.util.concurrent.ExecutionException;

import com.example.Module2.firebaseConfig.DataService;
import com.example.Module3.DashBoards.DashBoard;

import javafx.animation.FadeTransition;
import javafx.animation.RotateTransition;
import javafx.animation.ScaleTransition;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.transform.Rotate;
import javafx.stage.Stage;
import javafx.util.Duration;

public class loginPage {

    private Stage prStage;
    private Scene loginScene;
    private Scene userScene;
    private DataService dataService;
    public static String key;
    private landingPage2 landingPage2;

     /*
     * Author: Yashodip Thakare
       Desc: This class is responsible for creating and managing the' login page 
     */
    public loginPage(landingPage2 landingPage2) {
        this.landingPage2 = landingPage2;
        dataService = new DataService();
    }
     /*
     * Author : YASHODIP THAKARE
     * Desc : The method contains all the data and return Scene
     */
    public Scene initLoginScene(Stage prStage) {
        
         /*
     * Author : YASHODIP THAKARE
     * Desc : This pointer differntiate Global and Local variable
     */
        this.prStage = prStage;

          /*
     * Author : YASHODIP THAKARE
     * Desc : text Field for the UserName and PasswordField
     */
        TextField userField = new TextField();
        userField.setPromptText("Enter your Username");
        userField.setFont(new Font(17));
        userField.setPrefHeight(50);
        userField.setStyle("-fx-background-color: Transparent; -fx-border-style: solid; -fx-border-width: 0 0 2 0; ");


        PasswordField uPasswordField = new PasswordField();
        uPasswordField.setPromptText("Enter your Password");
        uPasswordField.setFont(new Font(17));
        uPasswordField.setPrefHeight(50);
        uPasswordField.setStyle("-fx-background-color: Transparent; -fx-border-style: solid; -fx-border-width: 0 0 2 0; ");


          /*
     * Author : YASHODIP THAKARE
     * Desc : login Aunthentication Button 
     */
        Button loginButton = new Button("Login");
        loginButton.setPrefSize(150, 60);
        loginButton.setStyle("-fx-background-color:coral;-fx-cursor:hand;-fx-border-radius: 5px");
        loginButton.setFont(Font.font("Noto Sans", FontWeight.BOLD, 20));
        loginButton.setTextFill(Color.WHITE);
        loginButton.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent event) {
                handleLogin(userField.getText(), uPasswordField.getText());
                String username = userField.getText();
                String password = uPasswordField.getText();
                handleLogin(username, password);

                // Display a dialog box with a message
                Dialog<String> dialog = new Dialog<String>();
                dialog.setTitle("Login");
                ButtonType type = new ButtonType("OK", ButtonData.OK_DONE);
                dialog.setContentText("");
                dialog.getDialogPane().getButtonTypes().add(type);
                dialog.setX(500);
                dialog.setY(400);
                if (key != null) {
                    dialog.setContentText("Login successful! Welcome, " + key);
                } else {
                    dialog.setContentText("Invalid credentials. Please try again.");
                }
                dialog.showAndWait();
            }
        });
         // Add mouse event handling for scaling effect
         loginButton.setOnMouseEntered(event -> {
            ScaleTransition scaleTransition = new ScaleTransition(Duration.millis(200), loginButton);
            scaleTransition.setToX(1.1); 
            scaleTransition.setToY(1.1); 
            scaleTransition.play();
        });

        loginButton.setOnMouseExited(event -> {
            ScaleTransition scaleTransition = new ScaleTransition(Duration.millis(200), loginButton);
            scaleTransition.setToX(1.0); 
            scaleTransition.setToY(1.0); 
            scaleTransition.play();
        });
  /*
     * Author : YASHODIP THAKARE
     * Desc : Three Labels contain different content
     */
        Label lb1 = new Label("Sign In To 4Wheels");
        lb1.setFont(Font.font("Noto Sans", FontWeight.BLACK, 40));

        Label lb2 = new Label("or use your account");
        lb2.setFont(Font.font("Arial", FontWeight.BOLD, 15));

        Label lb3 = new Label("forgot your Password ?");
        lb3.setFont(Font.font("Arial", FontWeight.BOLD, 15));

          /*
     * Author : YASHODIP THAKARE
     * Desc : Signup Button will redirect SignUp Page
     */
        Button signUpButton = new Button("SignUp");
        signUpButton.setStyle("-fx-background-color:TRANSPARENT");
        signUpButton.setFont(Font.font("Arial", FontWeight.BOLD, 20));
        signUpButton.setPrefSize(150, 40);
        signUpButton.setStyle("-fx-background-color:WHITE;-fx-cursor:hand ; -fx-textfill-color: coral");
        signUpButton.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent event) {
                showSignUpScene();
            }
        });
         // Add mouse event handling for scaling effect
         signUpButton.setOnMouseEntered(event -> {
            ScaleTransition scaleTransition = new ScaleTransition(Duration.millis(200), signUpButton);
            scaleTransition.setToX(1.1); 
            scaleTransition.setToY(1.1); 
            scaleTransition.play();
        });

        signUpButton.setOnMouseExited(event -> {
            ScaleTransition scaleTransition = new ScaleTransition(Duration.millis(200), signUpButton);
            scaleTransition.setToX(1.0); 
            scaleTransition.setToY(1.0); 
            scaleTransition.play();
        });

          /*
     * Author : YASHODIP THAKARE
     * Desc : 2 Hbox contains 2 Diiferrent Labels 
     */
        HBox hbSigninBox = new HBox(lb1);
        hbSigninBox.setAlignment(Pos.CENTER);

        HBox acHBox = new HBox(lb2);
        acHBox.setAlignment(Pos.CENTER);

         /*
     * Author : YASHODIP THAKARE
     * Desc : Assigning the images hardcoded
     */
        Image glImage = new Image("com/example/Assets/Google.png");
        ImageView glIV = new ImageView(glImage);
        glIV.setFitHeight(35);
        glIV.setFitWidth(40);

        Image fbImage = new Image("com/example/Assets/Fb.png");
        ImageView fbIV = new ImageView(fbImage);
        fbIV.setFitHeight(35);
        fbIV.setFitWidth(40);

        Image lnImage = new Image("com/example/Assets/Linkdin.png");
        ImageView lnIV = new ImageView(lnImage);
        lnIV.setFitHeight(35);
        lnIV.setFitWidth(40);

         /*
     * Author : YASHODIP THAKARE
     * Desc : Assigning the images to the VBox
     */
        VBox gleBox = new VBox(glIV);
        VBox fbBox = new VBox(fbIV);
        VBox lnBox = new VBox(lnIV);
 /*
     * Author : YASHODIP THAKARE
     */
        HBox logo = new HBox(15, gleBox, fbBox, lnBox);
        logo.setPrefHeight(40);
        logo.setAlignment(Pos.CENTER);

         /*
     * Author : YASHODIP THAKARE
     * Desc : It has the Label3 which is lb3 
     */
        HBox forgetBox = new HBox(lb3);
        forgetBox.setAlignment(Pos.CENTER);

         /*
     * Author : YASHODIP THAKARE
     * Desc : All the Vbox and Hbox passed to the login VBox
     */
        VBox vb1 = new VBox(hbSigninBox, logo, acHBox, userField, uPasswordField, forgetBox);
        vb1.setSpacing(30);

        HBox hblogin = new HBox(loginButton);
        hblogin.setAlignment(Pos.CENTER);

        VBox vb = new VBox(30, vb1, hblogin);
        vb.setAlignment(Pos.CENTER);
        vb.setPrefWidth(600);
        vb.setStyle(
                "-fx-padding: 50; -fx-background-color:WHITE;-fx-border-style: solid inside; -fx-border-width: 3; -fx-border-insets: 1; -fx-border-radius: 5; -fx-border-color:WHITE;");

        /*
        Author : YASHODIP THAKARE
                Label with text "Hello , Friend !"
                Label with text "Enter your personal detail"
                Label with text "start journey"
         */ 
        Label welcomLabel = new Label("Hello , Friend !");
        welcomLabel.setFont(Font.font("Noto Sans", FontWeight.BLACK, 40));
        welcomLabel.setStyle("-fx-text-fill:WHITE; -fx-font-weight: bold");

        Label sloganApp1 = new Label("Enter your personal details and");
        sloganApp1.setFont(Font.font("Arial", FontWeight.BOLD, 20));
        sloganApp1.setStyle("-fx-text-fill:WHITE;");

        Label sloganApp2 = new Label(" start journey with us ");
        sloganApp2.setFont(Font.font("Arial", FontWeight.BOLD, 20));
        sloganApp2.setStyle("-fx-text-fill:WHITE;");

        VBox info = new VBox(10, sloganApp1, sloganApp2);
        info.setAlignment(Pos.CENTER);

         /*
     * Author : YASHODIP THAKARE
     * Desc : Welcome Label and info adn SignUp button are assigned in the Vbox
     */
        VBox vbText = new VBox(50, welcomLabel, info, signUpButton);
        vbText.setAlignment(Pos.CENTER);
        vbText.setPrefWidth(400);
        vbText.setStyle("-fx-background-color: coral ");

         /*
     * Author : YASHODIP THAKARE
     * Desc : Hbox contain vb and vbText
     */
        HBox hbAll = new HBox(0, vb, vbText);
        hbAll.setAlignment(Pos.CENTER);
        hbAll.setPrefSize(1000, 400);
        hbAll.setStyle(
                "-fx-border-insets: 1 ; -fx-border-width: 2 ; -fx-border-color:Beige;");

         /*
     * Author : YASHODIP THAKARE
     * Desc : Image class for importing and Displaying 
     */
        Image brand = new Image("com/example/Assets/logo2.png");
        ImageView brandView = new ImageView(brand);
        brandView.setFitHeight(100);
        brandView.setFitWidth(100);
        brandView.setPreserveRatio(true);

         /*
     * Author : YASHODIP THAKARE
     * Desc : VBox for the BorderPane
     */
        VBox None1 = new VBox(brandView);
        None1.setPrefHeight(150);
        None1.setAlignment(Pos.TOP_CENTER);
        VBox None2 = new VBox();
        None2.setPrefWidth(200);

         /*
     * Author : YASHODIP THAKARE
     * Desc : Navigating Button
     */
        Button back = new Button("Back");
        back.setPrefSize(120, 40);
        back.setStyle("-fx-background-color:coral;-fx-cursor:hand;-fx-border-radius: 5px");
        back.setFont(Font.font("Noto Sans", FontWeight.BOLD, 20));
        back.setTextFill(Color.WHITE);
        back.setOnAction(new EventHandler<ActionEvent>(){
            public void handle(ActionEvent event){
               landingPage2.showLandingPage2scene();
            }
        });
         // Add mouse event handling for scaling effect
         back.setOnMouseEntered(event -> {
            ScaleTransition scaleTransition = new ScaleTransition(Duration.millis(200), back);
            scaleTransition.setToX(1.1); 
            scaleTransition.setToY(1.1); 
            scaleTransition.play();
        });

        back.setOnMouseExited(event -> {
            ScaleTransition scaleTransition = new ScaleTransition(Duration.millis(200), back);
            scaleTransition.setToX(1.0); 
            scaleTransition.setToY(1.0); 
            scaleTransition.play();
        });
 
        VBox None3 = new VBox(back);
        None3.setPrefHeight(150);
        VBox None4 = new VBox();
        None4.setPrefWidth(200);

         /*
     * Author : YASHODIP THAKARE
     * Desc : BorderPane provides center , top , right , bottom , left 
     */
        BorderPane bordPane = new BorderPane(hbAll, None1, None2, None3, None4);

        /*
     * Author : YASHODIP THAKARE
     * Desc : Add rotation to the logo and brandview
     */
        RotateTransition rotateTransition = new RotateTransition(Duration.seconds(10), logo);
        rotateTransition.setByAngle(360);
        rotateTransition.setCycleCount(RotateTransition.INDEFINITE);
        rotateTransition.setAutoReverse(false);
        rotateTransition.setAxis(Rotate.Y_AXIS);
        rotateTransition.play();

        RotateTransition rotateTransition2 = new RotateTransition(Duration.seconds(8), brandView);
        rotateTransition2.setByAngle(360);
        rotateTransition2.setCycleCount(RotateTransition.INDEFINITE);
        rotateTransition2.setAutoReverse(false);
        rotateTransition2.setAxis(Rotate.Y_AXIS);
        rotateTransition2.play();
        
        /*
     * Author : YASHODIP THAKARE
     * Desc : Add Scale image to hbAll and logo
     */ 
        ScaleTransition scaleTransition = new ScaleTransition(Duration.seconds(1), hbAll);
        scaleTransition.setFromX(0.5);
        scaleTransition.setFromY(0.5);
        scaleTransition.setToX(1);
        scaleTransition.setToY(1);
        scaleTransition.play();

        ScaleTransition logoScaleTransition = new ScaleTransition(Duration.seconds(2), logo);
        logoScaleTransition.setFromX(0.5);
        logoScaleTransition.setFromY(0.5);
        logoScaleTransition.setToX(1);
        logoScaleTransition.setToY(1);
        logoScaleTransition.play();

         /*
     * Author : YASHODIP THAKARE
     * Desc : Add fade transition to bordpane
     */
        FadeTransition fadeTransition = new FadeTransition(Duration.seconds(2), bordPane);
        fadeTransition.setFromValue(0);
        fadeTransition.setToValue(1);
        fadeTransition.play();

        return loginScene = new Scene(bordPane, 1400, 800);
    }

     /*
     * Author : YASHODIP THAKARE
     * Desc : Method to show login Scene
     */
    public void showLoginScene() {
        prStage.setScene(loginScene);
        prStage.setTitle("4Wheels");
        prStage.getIcons().add(new Image("com/example/Assets/logo2.png"));
        prStage.show();
    }
    /*
     * Author : YASHODIP THAKARE
     * Desc : Method to show SignUp Page 
     */
    private void showSignUpScene() {
        SignUpPage signupController = new SignUpPage(this); // Create SignupController instance
        Scene signupScene = signupController.createSignupScene(prStage); // Create signup scene
        prStage.getIcons().add(new Image("com/example/Assets/logo2.png"));
        prStage.setScene(signupScene);
        prStage.show();
    }

     /*
     * Author : YASHODIP THAKARE
     * Desc : Firebase authentication method after login clicked
     */
    private void handleLogin(String username, String password) {
        try {
            // Authenticate user
            if (dataService.authenticateUser(username, password)) {
                key = username; // Store the username in the static key
                DashBoard dash = new DashBoard(dataService, this) ;// Initialize user scene
                Scene scene = dash.dashboardScene(prStage);
                prStage.getIcons().add(new Image("com/example/Assets/logo2.png"));
                prStage.setScene(scene); // Show user scene
                prStage.setTitle("4Wheels");
            } else {
                System.out.println("Invalid credentials");
                key = null;
            }
        } catch (ExecutionException | InterruptedException ex) {
            ex.printStackTrace();
        }
    }

     /*
     * Author : YASHODIP THAKARE
     * Desc : Method to handle logout 
     */
    public void handleLogout() {
        prStage.setScene(loginScene); // Show login scene
        prStage.getIcons().add(new Image("com/example/Assets/logo2.png"));
        prStage.setTitle("4Wheels");
    }
}
