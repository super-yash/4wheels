package com.example.Module2.Controller;

import javafx.animation.FadeTransition;
import javafx.animation.PathTransition;
import javafx.animation.RotateTransition;
import javafx.animation.ScaleTransition;
import javafx.animation.TranslateTransition;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.effect.BoxBlur;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.LineTo;
import javafx.scene.shape.MoveTo;
import javafx.scene.shape.Path;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.transform.Rotate;
import javafx.stage.Stage;
import javafx.util.Duration;

public class landingPage1 {

    private Pane pane;
    private Scene scene;
    private Stage prStage;

    /*
     * Author: Yashodip Thakare
       Desc: This class is responsible for creating and managing the landing page 1 of an application.
     */
    public landingPage1(Stage prStage) {
        this.prStage = prStage;
        callLandingPage1();
    }

    /*
     * Author : Yashodip Thakare
     * Desc : Function coontain all the data , essential for Landing Page 1
     */
    public void callLandingPage1() {

        /*
         * Author : Yashodip Thakare
         * Desc : Labels :-
                * Home: A label with the text "Home".
                * Popular: A label with the text "Popular".
                * AboutUs: A label with the text "About Us".
                * Phone: A label with the text "Phone: +917666556478".
         */
        Label Home = new Label();
        Home.setText("Home");
        Home.setFont(Font.font("Verdana", FontWeight.BOLD, 20));
        Home.setStyle("-fx-cursor : hand; -fx-text-fill: BLACK");

        Label Popular = new Label();
        Popular.setText("Popular");
        Popular.setFont(Font.font("Verdana", FontWeight.BOLD, 20));
        Popular.setStyle("-fx-cursor : hand; -fx-text-fill: BLACK");

        Label AboutUs = new Label();
        AboutUs.setText("About Us");
        AboutUs.setFont(Font.font("Verdana", FontWeight.BOLD, 20));
        AboutUs.setStyle("-fx-cursor : hand; -fx-text-fill: BLACK");

        Label Phone = new Label();
        Phone.setText("Phone: +917666556478");
        Phone.setFont(Font.font("Verdana", FontWeight.BOLD, 20));
        Phone.setStyle("-fx-cursor : hand; -fx-text-fill: BLACK");

         /*
     * Author : Yashodip Thakare
     * Desc : A grid pane that contains the navigation labels (Home, Popular, AboutUs, Phone).
     */
        // GridPane gp = new GridPane();
        // gp.setHgap(30);
        // gp.add(Home, 0, 0);
        // gp.add(Popular, 1, 0);
        // gp.add(AboutUs, 2, 0);
        // gp.add(Phone, 3, 0);
        // gp.setLayoutX(480);
        // gp.setLayoutY(10);

           /*
     * Author : Yashodip Thakare
     * Desc : Navigation button for Accessing the next Page
     */
        Button startButton = new Button();
        startButton.setText("Get Started");
        startButton.setFont(Font.font("Arial", FontWeight.BOLD, 20));
        startButton.setStyle("-fx-background-color: coral ; -fx-cursor : Hand ; -fx-text-fill :White ;");
        startButton.setPrefSize(200, 50);
        startButton.setOpacity(1);

           /*
     * Author : Yashodip Thakare
     * Desc : Using inner class Concept , after clicking on StartButton the action of naviagating to another page will take place 
     */
        startButton.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent event) {
                handleLandingPage2();
            }
        });
         // Add mouse event handling for scaling effect
            startButton.setOnMouseEntered(event -> {
                ScaleTransition scaleTransition = new ScaleTransition(Duration.millis(200), startButton);
                scaleTransition.setToX(1.1); 
                scaleTransition.setToY(1.1); 
                scaleTransition.play();
            });

            startButton.setOnMouseExited(event -> {
                ScaleTransition scaleTransition = new ScaleTransition(Duration.millis(200), startButton);
                scaleTransition.setToX(1.0); 
                scaleTransition.setToY(1.0); 
                scaleTransition.play();
            });

             /*
     * Author : Yashodip Thakare
     * Desc : Horizontal Box conatins startButton "GetStarted".
     */
        HBox nextBox = new HBox(startButton);
        nextBox.setAlignment(Pos.CENTER);

                   /*
     * Author : Yashodip Thakare
     * Desc : Labels :-
     *      headline: A label with the text "Welcome to 4Wheels ,".
            headLabel: A label with the text "Your Car, Your Way !".
            subheadline: A label with the text "From compact cars to luxury SUVs, find the perfect vehicle to suit your lifestyle .".
     */
        Label headline = new Label();
        headline.setText("Welcome to 4Wheels ,");
        headline.setFont(Font.font("Arial", FontWeight.BOLD, 60));
        headline.setStyle("-fx-text-fill: BEIGE ; -fx-cursor : Hand ;");

        Label headLabel = new Label("Your Car, Your Way !");
        headLabel.setFont(Font.font("Arial", FontWeight.BOLD, 40));
        headLabel.setStyle("-fx-text-fill: BEIGE ; -fx-cursor : Hand ;");

        Label subheadline = new Label(
                "From compact cars to luxury SUVs, find the perfect vehicle to suit your lifestyle .");
        subheadline.setFont(Font.font("Arial", FontWeight.BOLD, 20));
        subheadline.setStyle("-fx-text-fill: BEIGE ; -fx-cursor : Hand ;");

               /*
     * Author : Yashodip Thakare
     * Desc : A vertical box that contains the headline, head label, and subheadline
     */
        VBox vbBox = new VBox(30, headline, headLabel, subheadline);

                       /*
     * Author : Yashodip Thakare
     * Desc :  A vertical box that contains the vbBox and nextBox
     */
        VBox vb = new VBox(100);
        vb.getChildren().addAll(vbBox, nextBox);
        vb.setLayoutX(50);
        vb.setLayoutY(250);

         /*
     * Author : Yashodip Thakare
     * Desc :  BoxBlur class is used to blur the images or any Box
     */
        BoxBlur bb = new BoxBlur();
        bb.setWidth(8);
        bb.setHeight(8);
        bb.setIterations(10);

         /*
     * Author : Yashodip Thakare
     * Desc : Import and place the images to thier correct place 
     */
        Image image = new Image("com/example/Assets/NatureCar.jpg");
        ImageView page1 = new ImageView(image);
        page1.setFitWidth(1480);
        page1.setFitHeight(880);
        page1.setEffect(bb);

        Image brand = new Image("com/example/Assets/logo2.png");
        ImageView brandView = new ImageView(brand);
        brandView.setFitHeight(100);
        brandView.setFitWidth(100);
        brandView.setLayoutX(50);
        brandView.setLayoutY(20);
        brandView.setPreserveRatio(true);

         /*
     * Author : Yashodip Thakare
     * Desc : Rotation Animation , rotates the brand logo 
     */
        RotateTransition rotateTransition2 = new RotateTransition(Duration.seconds(15), brandView);
        rotateTransition2.setByAngle(360);
        rotateTransition2.setCycleCount(RotateTransition.INDEFINITE);
        rotateTransition2.setAutoReverse(false);
        rotateTransition2.setAxis(Rotate.Y_AXIS);
        rotateTransition2.play();

         /*
     * Author : Yashodip Thakare
     * Desc : Fades in the headLabel and subheadline labels.
     */
        FadeTransition fadeTransition = new FadeTransition(Duration.seconds(4), headLabel);
        fadeTransition.setFromValue(0);
        fadeTransition.setToValue(1);
        fadeTransition.play();

        FadeTransition fadeTransition2 = new FadeTransition(Duration.seconds(4), subheadline);
        fadeTransition2.setFromValue(0);
        fadeTransition2.setToValue(1);
        fadeTransition2.setDelay(Duration.seconds(1));
        fadeTransition2.play();

           /*
     * Author : Yashodip Thakare
     * Desc : Translates the "Get Started" button from right to left.
     */
        TranslateTransition translateTransition = new TranslateTransition(Duration.seconds(4), startButton);
        translateTransition.setFromX(200);
        translateTransition.setToX(0);
        translateTransition.play();

         /*
     * Author : Yashodip Thakare
     * Desc :   A pane that contains all the components of the landing page 1.
     */
        // Path path = new Path();
        // path.getElements().add(new MoveTo(480, 10));
        // path.getElements().add(new LineTo(480, 50));
        // path.getElements().add(new LineTo(480, 10));
        // PathTransition pathTransition = new PathTransition(Duration.seconds(3), path, gp);
        // pathTransition.setCycleCount(RotateTransition.INDEFINITE);
        // pathTransition.play();

        
        pane = new Pane(page1, brandView, vb);

        scene = new Scene(pane, 1400, 800);
        scene.setFill(Color.BEIGE);

    }

     /*
     * Author: Yashodip Thakare
       Desc: Function contains all the data essential for Landing Page 1.
             InitLandingPage1(): Returns the scene of the landing page 1.
     */
    public Scene InitLandingPage1() {
        return scene;
    }

      /*
     * Author: Yashodip Thakare
       Desc: Returns the scene of the landing page 1.
             showLandingPage1scene(): Sets the scene of the provided stage to the landing page 1 and shows it.
     */
    public void showLandingPage1scene() {
        prStage.setScene(scene);
        prStage.getIcons().add(new Image("com/example/Assets/logo2.png"));
        prStage.show();
    }

     /*
     * Author: Yashodip Thakare
       Desc: Sets the scene of the provided stage to the landing page 1 and shows it.
             handleLandingPage2(): Handles the event of clicking the "Get Started" button, which navigates to the landing page 2.
     */
    public void handleLandingPage2() {
        landingPage2 controllLandingPage2 = new landingPage2(this);
        Scene newScene = controllLandingPage2.callLandingPage2(prStage);
        prStage.setScene(newScene);
        prStage.getIcons().add(new Image("com/example/Assets/logo2.png"));
        prStage.show();
    }

}