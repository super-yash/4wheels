package com.example.Module3.DashBoards;

import javafx.animation.PathTransition;
import javafx.animation.RotateTransition;
import javafx.animation.ScaleTransition;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.shape.LineTo;
import javafx.scene.shape.MoveTo;
import javafx.scene.shape.Path;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;
import javafx.util.Duration;

public class aboutus {
  private Scene scene;
  private Stage prStage;
  private DashBoard dashBoard;

  public aboutus(DashBoard dashBoard) {
    this.dashBoard = dashBoard;
  }

  public Scene callAboutUs(Stage prStage) {

    this.prStage = prStage;

    Button back = new Button();

    Image logo = new Image("com/example/Assets/back-button.png");
    ImageView logoView = new ImageView(logo);
    logoView.setFitHeight(50);
    logoView.setFitWidth(50);

    back.setOnAction(event -> {
      DashBoard.showDashBoardscene();
    });
    back.setGraphic(logoView);
    back.setLayoutX(30);
    back.setLayoutY(10);

    Label aboutlabel = new Label("ABOUT US");
    aboutlabel.setFont(Font.font("Times New Roman", FontWeight.BOLD, 40));

    Path path = new Path();
    path.getElements().add(new MoveTo(700, 15));
    path.getElements().add(new LineTo(700, 40));
    path.getElements().add(new LineTo(700, 15));
    PathTransition pathTransition = new PathTransition(Duration.seconds(3), path, aboutlabel);
    pathTransition.setCycleCount(RotateTransition.INDEFINITE);
    pathTransition.play();

    Image picImage = new Image("com/example/Assets/ShashiSir.png");
    ImageView piclogoView = new ImageView(picImage);

    Label label1 = new Label("                                 Shashikant Sir");
    label1.setFont(Font.font("Times New Roman", FontWeight.BOLD, 40));
    label1.setStyle("-fx-text-fill: maroon ");

    ScaleTransition scaleTransition = new ScaleTransition(Duration.seconds(3), label1);
    scaleTransition.setFromX(0.5);
    scaleTransition.setFromY(0.5);
    scaleTransition.setToX(1);
    scaleTransition.setToY(1);
    scaleTransition.play();

    Label label2 = new Label();
    label2.setText(
        "   we are writing to express our deepest gratitude and appreciation \n   for the remarkable contributions you have made.Your insights \n   and expertise on JAVA and SuperX have been truly enlightening \n   and have left a profound impact on all of us.\n"
            + //
            "\n" + //
            "             Your dedication, hard work, and unwavering commitment to \n   excellence are truly inspiring.Your guidance and support have been \n   instrumental in our growth and success."
            + //
            "\n" + //
            "   Thank you, Sir, and thanks to everyone who guided us to build the \n   project.");
    label2.setFont(Font.font("Times New Roman", FontWeight.BOLD, 30));
    label2.setTextAlignment(TextAlignment.JUSTIFY);

    ScaleTransition label2ScaleTransition = new ScaleTransition(Duration.seconds(2), label2);
    label2ScaleTransition.setFromX(0.5);
    label2ScaleTransition.setFromY(0.5);
    label2ScaleTransition.setToX(1);
    label2ScaleTransition.setToY(1);
    label2ScaleTransition.play();

    VBox vBox3 = new VBox(20, label1, label2);
    vBox3.setPrefSize(1000, 200);
    vBox3.setLayoutY(400);

    VBox vBox4 = new VBox(piclogoView);
    vBox4.setAlignment(Pos.TOP_RIGHT);

    vBox4.setPrefSize(400, 400);

    Label lowerLabel = new Label();
    lowerLabel.setText("Project By :- 1) Yahodip Thakare" + "\n" +
        "                      2) Vaibhav Patil ");
    lowerLabel.setFont(Font.font("Times New Roman", FontWeight.BOLD, 25));
    lowerLabel.setLayoutX(150);
    lowerLabel.setLayoutY(600);
    lowerLabel.setStyle("-fx-text-fill: brown ");

    ScaleTransition labelScaleTransition = new ScaleTransition(Duration.seconds(2), lowerLabel);
    labelScaleTransition.setFromX(0.5);
    labelScaleTransition.setFromY(0.5);
    labelScaleTransition.setToX(1);
    labelScaleTransition.setToY(1);
    labelScaleTransition.play();

    HBox hBox1 = new HBox(40, vBox3, vBox4);
    hBox1.setPrefHeight(600);
    hBox1.setPrefWidth(1400);
    hBox1.setLayoutY(90);

    Pane pane = new Pane(logoView, aboutlabel, hBox1, lowerLabel);
    return scene = new Scene(pane, 1400, 800);
  }

}
