Project for java FX
 Project Name :  4Wheels
When user want to buy the new car then we will show the appropriate cars for user. And our concept is suitable for the user to access the car information at different location.
 UI Pages 
1.	First  two page is landing page 
2.	Login and Signup page for user
3.	Home Page
4.	Dashboard Page
5.	About us page 
PROJECT IDEAS
Basically the idea behind to developing this project is to when user want to purchase new car then he/she will be confused which car should buy. So we provide way that  user can find the trending cars and popular cars. Which is beneficial for their investment.
Components
1.	Landing Page1
•	 Attractive UI for the User
•	 Get Started Button
2.	Landing Page2
•	Information about why to choose us
•	 Customer review
3.	Login and SignUp Page
•	User registration
•	User authentication
4.	Home page
Overview of user that can be observe the cars and decide which can be best to                
Buy.
•	User get the information about the cars
5.	DashBoard Page
•	Features: In the dashboard page upload the images and information about popular cars and trending cars and car name and price

6.	About Page
•	Our Gratitude to Shashi Sir and Sachin Sir and all the respective Mentors.
Technologies
Frontend (JavaFX)
1.	JavaFX
•	UI components and layout.
•	Scene Builder: For designing the UI.
2.	Libraries
•	JavaFX Scene Builder : For design JavaFX application interfaces.
•	JavaFX CSS  : Styling the JavaFX application.
Backend (Java)
1.	Services
•	Dependency Injection, Firebstore, API (location)
2.	 Security
•	User authentication and authorization.

3.	Database
•	Firebase Database: For storing user data
Project Structure
Frontend (JavaFX)
1.	Classes
•	Each page will have a corresponding  class.
•	Example: Login, Dashboard.
2.	FXML Files
•	UI layout files.
•	Example: login.fxml, dashboard.fxml, chat.fxml.
3.	Models
•	Represent the data structure.
•	Example: User, Buyer
4.	Services
•	Use Firestore to store the data
•	And location API

Backend (Spring Boot)
1.	Controllers
•	Handle HTTP requests.
•	Example: AuthController
2.	Services
•	Business logic.
•	Example: UserService
3.	Repositories
•	Database operations.
•	Example: UserRepository
4.	Entities
•	Database entity models.
•	Example: User in the form of Buyer
5.	Security Configuration
•	Configuring Firebase Authentication

.
