package com.example.Module3.DashBoards;

import com.example.Module2.Controller.loginPage;
import com.example.Module2.firebaseConfig.DataService;
import com.example.Module3.Module4.PUNCH;
import com.example.Module3.Module4.SCORPIO;
import com.example.Module3.Module4.SWIFT;
import com.example.Module3.Module4.THAR;
import com.google.cloud.firestore.DocumentSnapshot;

import javafx.animation.RotateTransition;
import javafx.animation.ScaleTransition;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.transform.Rotate;
import javafx.stage.Stage;
import javafx.util.Duration;

public class DashBoard {
    private DataService dataService;
    public static String userName;
    private static Scene DSscene ;
    private loginPage loginPage;
    private static Stage prStage;
    private Dash2 dash2;
    private Dash3 dash3;

    public DashBoard(DataService dataService,loginPage loginPage) {
        this.dataService = dataService;
        this.loginPage = loginPage;
    }

    public Scene dashboardScene(Stage prStage) {

        DashBoard.prStage = prStage ;

        try {
            String key = loginPage.key; // Get the username key from LoginController
            System.out.println("Value of key: " + key);
            DocumentSnapshot dataObject = dataService.getData("users", key);
            userName = dataObject.getString("username"); // Fetch the username from the dataObject

            System.out.println("username fetched: " + userName);
            // dataLabel.setText( userName); // Set the username in the label
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        // // Set action for the logout button
        // logoutButton.setOnAction(new EventHandler<ActionEvent>() {
        //     @Override
        //     public void handle(ActionEvent event) {
        //         loginPage.handleLogout(); // Run the logout handler
        //     }
        // });
     /**
         * AuthorName :Vaibhav Patil
         * Desc: Adding logo image and set the css properties
         */
       Image logo = new Image("com/example/Assets/logo2.png");
    ImageView logoView = new ImageView(logo);
    logoView.setFitHeight(80);
    logoView.setFitWidth(80);

    DropShadow ds = new DropShadow();
    ds.setOffsetY(3.0f);
    ds.setColor(Color.color(0.4f, 0.4f, 0.4f));
/**
         * AuthorName :Vaibhav Patil
         * Desc: Create buttons on  headerbar and applying css properties
         */
    Button homeButton = new Button("Home");
    homeButton.setEffect(ds);
    // homeButton.setOnAction(new EventHandler<ActionEvent>() {
    //   public void handle(ActionEvent event) {

    //   }
    // });
    homeButton.setFont(Font.font("Verdana", FontWeight.BOLD, 20));
    homeButton.setStyle("-fx-background-color: beige; -fx-cursor:hand ; -fx-text-fill : black;");
    homeButton.setPrefSize(180, 50);

    Button popularcarbutton = new Button("PopularCars");
    popularcarbutton.setOnAction(new EventHandler<ActionEvent>() {
      public void handle(ActionEvent event) {
        handleDash2Page();
        popularcarbutton.setEffect(ds);
       // handleDash2Page();
      }
    });
    popularcarbutton.setFont(Font.font("Verdana", FontWeight.BOLD, 20));
    popularcarbutton.setStyle("-fx-background-color:beige;-fx-cursor:hand ; -fx-text-fill : black;");
    popularcarbutton.setPrefSize(180, 50);

    Button trendingbutton = new Button("TrendingCars");
    trendingbutton.setOnAction(new EventHandler<ActionEvent>() {
      public void handle(ActionEvent event) {
        handleDash3Page();
        trendingbutton.setEffect(ds);
        //Dash3.initDash3Page(prStage);
      }
    });
    trendingbutton.setFont(Font.font("Verdana", FontWeight.BOLD, 20));
    trendingbutton.setStyle("-fx-background-color:beige;-fx-cursor:hand; -fx-text-fill : black");
    trendingbutton.setPrefSize(180, 50);

    Button aboutBT = new Button("AboutUs");
    aboutBT.setOnAction(new EventHandler<ActionEvent>() {
      public void handle(ActionEvent event) {
        handleAboutUsPage();
        aboutBT.setEffect(ds);
        //Dash3.initDash3Page(prStage);
      }
    });
    aboutBT.setFont(Font.font("Verdana", FontWeight.BOLD, 20));
    aboutBT.setStyle("-fx-background-color:beige;-fx-cursor:hand; -fx-text-fill : black");
    aboutBT.setPrefSize(180, 50);

    
    Button locationbutton = new Button();
    locationbutton.setOnAction(new EventHandler<ActionEvent>() {
      public void handle(ActionEvent event) {
        SearchLocation.showLocationSearch();
      }
    });
     // Add mouse event handling for scaling effect
     locationbutton.setOnMouseEntered(event -> {
      ScaleTransition scaleTransition = new ScaleTransition(Duration.millis(200), locationbutton);
      scaleTransition.setToX(1.1); 
      scaleTransition.setToY(1.1); 
      scaleTransition.play();
  });

  locationbutton.setOnMouseExited(event -> {
      ScaleTransition scaleTransition = new ScaleTransition(Duration.millis(200), locationbutton);
      scaleTransition.setToX(1.0); 
      scaleTransition.setToY(1.0); 
      scaleTransition.play();
  });
     /**
         * AuthorName :Vaibhav Patil
         * Desc: Adding locadtionlogo image and set the css properties
         */

    Image locationImage = new Image("com/example/Assets/pinLoc.png");
    ImageView locationImageView = new ImageView(locationImage);
    locationImageView.setFitHeight(40);
    locationImageView.setFitWidth(40);
    locationbutton.setGraphic(locationImageView);
    locationbutton.setStyle("-fx-background-color:beige;-fx-cursor:hand; -fx-text-fill : black");
    locationbutton.setFont(Font.font("Verdana", FontWeight.BOLD, 20));

    Button logoutbutton = new Button("Logout");
    logoutbutton.setOnAction(new EventHandler<ActionEvent>() {
      public void handle(ActionEvent event) {
        loginPage.handleLogout(); // Run the logout handler
        logoutbutton.setEffect(ds);
      }
    });
     // Add mouse event handling for scaling effect
     logoutbutton.setOnMouseEntered(event -> {
      ScaleTransition scaleTransition = new ScaleTransition(Duration.millis(200), logoutbutton);
      scaleTransition.setToX(1.1); 
      scaleTransition.setToY(1.1); 
      scaleTransition.play();
  });

  logoutbutton.setOnMouseExited(event -> {
      ScaleTransition scaleTransition = new ScaleTransition(Duration.millis(200), logoutbutton);
      scaleTransition.setToX(1.0); 
      scaleTransition.setToY(1.0); 
      scaleTransition.play();
  });
    logoutbutton.setFont(Font.font("Verdana", FontWeight.BOLD, 20));
    logoutbutton.setStyle("-fx-background-color:beige;-fx-cursor:hand ; -fx-text-fill : black");
    // navButton.setPrefSize(150,50);

    /**
         * AuthorName :Vaibhav Patil
         * Desc: Adding all buttons and labels to hbox
         */
    HBox log_locBox = new HBox(20,aboutBT, locationbutton, logoutbutton);
    // log_locBox.setPrefWidth(200);
    log_locBox.setAlignment(Pos.CENTER);

    HBox headerbar = new HBox(50, logoView, homeButton, popularcarbutton, trendingbutton);
    headerbar.setAlignment(Pos.CENTER);
    headerbar.setPrefWidth(800);
    headerbar.setStyle("-fx-background-color: Beige");
    
    RotateTransition rotateTransition2 = new RotateTransition(Duration.seconds(15), logoView);
        rotateTransition2.setByAngle(360);
        rotateTransition2.setCycleCount(RotateTransition.INDEFINITE);
        rotateTransition2.setAutoReverse(false);
        rotateTransition2.setAxis(Rotate.Y_AXIS);
        rotateTransition2.play();

    HBox tophead = new HBox(200, headerbar, log_locBox);
    tophead.setStyle("-fx-background-color: Beige");
    tophead.setPrefSize(1400, 100);

    ScrollPane carpane = SlidingWindow.createSlidingImageWindow();

     /**
         * AuthorName :Vaibhav Patil
         * Desc: Creating labels 
         */
    Label lb1 = new Label("Welcome to 4Wheels..");
    lb1.setFont(Font.font("sans-serif", FontWeight.BOLD, 40));

    Label lb2 = new Label("Drive Smarter, Drive Better");
    lb2.setFont(Font.font("sans-serif", FontWeight.BOLD, 30));
    lb2.setAlignment(Pos.TOP_RIGHT);

    Label lb3 = new Label("A Bundle of Joy on Four Wheels");
    lb3.setFont(Font.font("sans-serif", FontWeight.BOLD, 25));

    Label lb4 = new Label("Drive with Power, Conquer the Road");
    lb4.setFont(Font.font("sans-serif", FontWeight.BOLD, 23));

    Label lb5 = new Label("Feel the Rush, Drive with Confidence");
    lb5.setFont(Font.font("sans-serif", FontWeight.BOLD, 20));

    VBox sloganBox = new VBox(20, lb2, lb3, lb4, lb5);

    VBox leftVBox = new VBox(50, lb1, sloganBox);
    leftVBox.setPrefSize(480, 50);
    leftVBox.setStyle("-fx-padding : 30; ");

    ScaleTransition scaleTransition = new ScaleTransition(Duration.seconds(4), leftVBox);
    scaleTransition.setFromX(1.0);
    scaleTransition.setToX(0.80);
    scaleTransition.setFromY(1.0);
    scaleTransition.setToY(0.80);
    scaleTransition.setCycleCount(ScaleTransition.INDEFINITE);
    scaleTransition.setAutoReverse(true);
    scaleTransition.play();

    VBox rightVBox = new VBox(carpane);
    rightVBox.setPrefSize(500, 240);

    HBox middleBox = new HBox(200, leftVBox, rightVBox);
    middleBox.setPrefSize(1400, 360);
    middleBox.setAlignment(Pos.CENTER);
    middleBox.setStyle(" -fx-padding : 10 ");
    middleBox.setLayoutY(100);

    Image image1 = new Image("com/example/Assets/THAR.jpeg");
    ImageView imageView1 = new ImageView(image1);
    imageView1.setFitHeight(160);
    imageView1.setFitWidth(325);

    Image image2 = new Image("com/example/Assets/SWIFT.jpeg");
    ImageView imageView2 = new ImageView(image2);
    imageView2.setFitHeight(160);
    imageView2.setFitWidth(325);

    Image image3 = new Image("com/example/Assets/SCORPIO.jpeg");
    ImageView imageView3 = new ImageView(image3);
    imageView3.setFitHeight(160);
    imageView3.setFitWidth(325);

    Image image4 = new Image("com/example/Assets/PUNCH.jpeg");
    ImageView imageView4 = new ImageView(image4);
    imageView4.setFitHeight(160);
    imageView4.setFitWidth(325);

    /**
         * AuthorName :Vaibhav Patil
         * Desc: Creating buttons for image when we click imageshow
         */
    Button bottomButton1 = new Button();
    Image clickImage = new Image("com/example/Assets/click.png");
    ImageView clickImageView = new ImageView(clickImage);
    clickImageView.setFitHeight(40);
    clickImageView.setFitWidth(40);
    bottomButton1.setGraphic(clickImageView);
    bottomButton1
    .setStyle("-fx-font-size: 14px; -fx-font-weight:bold ; -fx-cursor:hand; -fx-background-color:Transparent");
    bottomButton1.setOnAction(new EventHandler<ActionEvent>() {
      public void handle(ActionEvent event) {
        System.out.println("Button Clicked");
        handleTHAR();
      }
    });

   

    Button bottomButton2 = new Button();
    Image clickImage2 = new Image("com/example/Assets/click.png");
    ImageView clickImageView2 = new ImageView(clickImage2);
    clickImageView2.setFitHeight(40);
    clickImageView2.setFitWidth(40);
    bottomButton2.setGraphic(clickImageView2);
    bottomButton2
        .setStyle("-fx-font-size: 14px; -fx-font-weight:bold ; -fx-cursor:hand; -fx-background-color:Transparent");
    bottomButton2.setOnAction(new EventHandler<ActionEvent>() {
      public void handle(ActionEvent event) {
        System.out.println("Button Clicked");
        handleSwift();
      }
    });

    Button bottomButton3 = new Button();
    Image clickImage3 = new Image("com/example/Assets/click.png");
    ImageView clickImageView3 = new ImageView(clickImage3);
    clickImageView3.setFitHeight(40);
    clickImageView3.setFitWidth(40);
    bottomButton3.setGraphic(clickImageView3);
    bottomButton3
    .setStyle("-fx-font-size: 14px; -fx-font-weight:bold ; -fx-cursor:hand; -fx-background-color:Transparent");
    bottomButton3.setOnAction(new EventHandler<ActionEvent>() {
      public void handle(ActionEvent event) {
        System.out.println("Button Clicked");
        handleSCORPIO();
      }
    });


    Button bottomButton4 = new Button();
    Image clickImage4 = new Image("com/example/Assets/click.png");
    ImageView clickImageView4 = new ImageView(clickImage4);
    clickImageView4.setFitHeight(40);
    clickImageView4.setFitWidth(40);
    bottomButton4.setGraphic(clickImageView4);
    bottomButton4
        .setStyle("-fx-font-size: 14px; -fx-font-weight:bold ; -fx-cursor:hand; -fx-background-color:Transparent");

    bottomButton4.setOnAction(new EventHandler<ActionEvent>() {
      public void handle(ActionEvent event) {
        System.out.println("Button Clicked");
        handlePUNCH();
      }
    });
       /**
         * AuthorName :Vaibhav Patil
         * Desc: Creating labels 
         */

    Label namelb1 = new Label("Mahindra Thar");
    namelb1.setFont(Font.font("sans-serif", FontWeight.BOLD, 22));
    Label namelb2 = new Label("Maruti Swift");
    namelb2.setFont(Font.font("sans-serif", FontWeight.BOLD, 22));
    Label namelb3 = new Label("Mahindra Scorpio");
    namelb3.setFont(Font.font("sans-serif", FontWeight.BOLD, 22));
    Label namelb4 = new Label("Tata Punch");
    namelb4.setFont(Font.font("sans-serif", FontWeight.BOLD, 22));

    VBox indside1_lower = new VBox(15, namelb1, imageView1, bottomButton1);
    indside1_lower.setPrefSize(325, 100);
    indside1_lower.setAlignment(Pos.CENTER);
    // indside1_lower.setStyle("-fx-background-color: yellow; ");

    VBox indside2_lower = new VBox(15, namelb2, imageView2, bottomButton2);
    indside2_lower.setPrefSize(325, 100);
    indside2_lower.setAlignment(Pos.CENTER);
    // indside2_lower.setStyle("-fx-background-color: green; ");

    VBox indside3_lower = new VBox(15, namelb3, imageView3, bottomButton3);
    indside3_lower.setPrefSize(325, 100);
    indside3_lower.setAlignment(Pos.CENTER);
    // indside3_lower.setStyle("-fx-background-color: red; ");

    VBox indside4_lower = new VBox(15, namelb4, imageView4, bottomButton4);
    indside4_lower.setPrefSize(325, 100);
    indside4_lower.setPrefHeight(20);
    indside4_lower.setAlignment(Pos.CENTER);
    // indside4_lower.setStyle("-fx-background-color: pink; ");

    HBox lowerBox = new HBox(20, indside1_lower, indside2_lower, indside3_lower, indside4_lower);
    lowerBox.setStyle(" -fx-padding : 30 30 30 30");
    lowerBox.setPrefSize(1400, 350);
    lowerBox.setLayoutY(460);
    lowerBox.setAlignment(Pos.CENTER);

    Pane pane = new Pane(tophead, middleBox, lowerBox);
    pane.setStyle("-fx-background-color: white ");
 DSscene = new Scene(pane, 1400, 800);

         return DSscene ;
    }

     public static void showDashBoardscene() {
        prStage.setScene(DSscene);
        prStage.getIcons().add(new Image("com/example/Assets/logo2.png"));
        prStage.show();
    }

    public void handleDash2Page() {
            Dash2 controllDash2Page = new Dash2(this,loginPage);
            Scene Dash2scene = controllDash2Page.callDash2(prStage);
            prStage.setScene(Dash2scene);
            prStage.getIcons().add(new Image("com/example/Assets/logo2.png"));
            prStage.show();
        }
      
        public void handleDash3Page() {
          Dash3 controllDash3 = new Dash3(this,dash2,loginPage);
          Scene sceneDash3 = controllDash3.callDash3(prStage);
          prStage.setScene(sceneDash3);
          prStage.getIcons().add(new Image("com/example/Assets/logo2.png"));
          prStage.show();
      }

      public void handleAboutUsPage() {
        aboutus controllAboutUS = new aboutus(this);
        Scene sceneAboutUs = controllAboutUS.callAboutUs(prStage);
        prStage.setScene(sceneAboutUs);
        prStage.getIcons().add(new Image("com/example/Assets/logo2.png"));
        prStage.show();
    }

    public void handleCarInfo() {
      carInfo controllCarInfo = new carInfo(this,dash2,dash3);
      Scene carinfoScene = controllCarInfo.callCarInfo(prStage);
      prStage.setScene(carinfoScene);
      prStage.show();
  }

  public void handleTHAR() {
    THAR controllTHAR = new THAR(this);
    Scene THARScene = controllTHAR.callTHAR(prStage);
    prStage.setScene(THARScene);
    prStage.show();
}
      public void handleSwift() {
      SWIFT controllSWIFT = new SWIFT(this);
      Scene SWIFTScene = controllSWIFT.callSWIFT(prStage);
      prStage.setScene(SWIFTScene);
      prStage.show();
  }

  public void handleSCORPIO() {
    SCORPIO controllSCORPIO = new SCORPIO(this);
    Scene SCORPIOScene = controllSCORPIO.callSCORPIO(prStage);
    prStage.setScene(SCORPIOScene);
    prStage.show();
}

public void handlePUNCH() {
  PUNCH controllPUNCH = new PUNCH(this);
  Scene PUNCHScene = controllPUNCH.callPUNCH(prStage);
  prStage.setScene(PUNCHScene);
  prStage.show();
}
    
  }