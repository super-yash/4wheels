package com.example.Module3.DashBoards;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.scene.Cursor;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.util.Duration;

public class SlidingWindow {
  private static final int IMAGE_HEIGHT = 350; // Height of the images

   public static ScrollPane createSlidingImageWindow() {
        // Create an HBox to hold the images
        HBox image_Box = new HBox(100);

        // Hardcoded images
        Image[] images = new Image[] {
               new Image("com/example/Assets/THAR.jpeg"),
                new Image("com/example/Assets/SWIFT.jpeg"),
                new Image("com/example/Assets/SCORPIO.jpeg"),
                new Image("com/example/Assets/PUNCH.jpeg"),
                new Image("com/example/Assets/THAR.jpeg"),
                new Image("com/example/Assets/PUNCH.jpeg"),
               
        };

        // Add images and their duplicates to the HBox
        for (Image image : images) {
            ImageView imageView = new ImageView(image);
            imageView.setFitHeight(IMAGE_HEIGHT);
            imageView.setFitWidth(500);
            image_Box.setCursor(Cursor.HAND);
            image_Box.getChildren().add(imageView);
            image_Box.setStyle("-fx-border-radius: 15px");
         
        }

        // Create a ScrollPane and set the HBox as its content
        ScrollPane scrollPane = new ScrollPane();
        scrollPane.setContent(image_Box);
        scrollPane.setFitToHeight(true); // Fit the height of the scroll pane to its content
        scrollPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER); // Hide the horizontal scrollbar
        scrollPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.NEVER); // Hide the vertical scrollbar

        KeyFrame keyFrame = new KeyFrame(Duration.millis(2000), event -> {
            // Check if the scrollPane has reached or exceeded its maximum Hvalue
            if (scrollPane.getHvalue() >= 1.0) {
                // Reset to start
                scrollPane.setHvalue(0);
            } else {
                // Scroll to the next image
                scrollPane.setHvalue(scrollPane.getHvalue() + 0.2);
            }
        });

        // Create a Timeline for automatic sliding
        Timeline timeline = new Timeline(keyFrame);
        timeline.setCycleCount(Timeline.INDEFINITE); // Repeat indefinitely
        timeline.play(); // Start the animation
    
       return scrollPane;
   }
    
}



