package com.example.Module3.DashBoards;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class carInfo {
    private static Stage prStage ; 
    private static Scene scene ;
    private DashBoard dashBoard ; 
    private Dash2 dash2 ; 
    private Dash3 dash3 ;

  // Author: Yashodip Thakare
// Desc: A constructor for the carInfo class
public carInfo(DashBoard dashBoard, Dash2 dash2, Dash3 dash3) {
    // Initialize the dashBoard, dash2, and dash3 fields
    this.dashBoard = dashBoard;
    this.dash2 = dash2;
    this.dash3 = dash3;
}

// Author: Yashodip Thakare
// Desc: A method to create a scene for car information
public Scene callCarInfo(Stage prStage) {
    // Set the primary stage
    this.prStage = prStage;

    // Create a back button
    Button back = new Button();
    back.setText("Back");
    back.setOnAction(e -> {
        // When the back button is clicked, show the dashboard scene
        DashBoard.showDashBoardscene();
    });
    back.setPrefSize(100, 100);
    back.setStyle("-fx-background-color: transparent");

    // Load the coming soon image
    Image comingSoonimage = new Image("com/example/Assets/comingSoon.jpg");
    ImageView cominImageView = new ImageView(comingSoonimage);
    cominImageView.setFitHeight(800);
    cominImageView.setFitWidth(1400);

    // Create a StackPane to hold the image and back button
    StackPane sp = new StackPane();
    sp.getChildren().addAll(cominImageView, back);

    // Create a VBox to hold the StackPane
    VBox vbox = new VBox(sp);
    vbox.setAlignment(Pos.CENTER);

    // Create a scene with the VBox
    scene = new Scene(vbox, 1400, 800);
    return scene;
}

}
