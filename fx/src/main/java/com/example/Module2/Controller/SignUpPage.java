package com.example.Module2.Controller;

import java.util.HashMap;
import java.util.Map;

import com.example.Module2.firebaseConfig.DataService;

import javafx.animation.FadeTransition;
import javafx.animation.RotateTransition;
import javafx.animation.ScaleTransition;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.transform.Rotate;
import javafx.stage.Stage;
import javafx.util.Duration;

public class SignUpPage {

  private loginPage loginPage;

  public SignUpPage(loginPage loginPage) {
    this.loginPage = loginPage;
  }

  public Scene createSignupScene(Stage prStage) {

    Label AcCreate = new Label("Sign Up to 4Wheels");
    AcCreate.setFont(Font.font("Noto Sans", FontWeight.BOLD, 40));

    Label createInfo = new Label(" or use your email for registration");
    createInfo.setFont(Font.font("Arial", FontWeight.BOLD, 15));

    TextField userName = new TextField();
    userName.setPromptText("Enter Your UserName");
    userName.setFont(new Font(17));
    userName.setPrefHeight(50);
    userName.setStyle("-fx-background-color: Transparent; -fx-border-style: solid; -fx-border-width: 0 0 2 0; ");


    TextField userEmail = new TextField();
    userEmail.setPromptText("Enter Your Email Id");
    userEmail.setFont(new Font(17));
    userEmail.setPrefHeight(50);
    userEmail.setStyle("-fx-background-color: Transparent; -fx-border-style: solid; -fx-border-width: 0 0 2 0; ");


    PasswordField userPassword = new PasswordField();
    userPassword.setPromptText("Enter Your Password");
    userPassword.setFont(new Font(17));
    userPassword.setPrefHeight(50);
    userPassword.setStyle("-fx-background-color: Transparent; -fx-border-style: solid; -fx-border-width: 0 0 2 0; ");


    Button signup = new Button("Sign Up");
    signup.setStyle("-fx-background-color:coral; -fx-cursor: hand;");
    signup.setFont(Font.font("Verdana", FontWeight.BOLD, 20));
    signup.setPrefSize(150, 60);
    signup.setTextFill(Color.WHITE);
    signup.setOnAction(new EventHandler<ActionEvent>() {
      public void handle(ActionEvent event) {
        handleSignup(prStage, userName.getText(), userPassword.getText() , userEmail.getText());
      }
    });
     // Add mouse event handling for scaling effect
     signup.setOnMouseEntered(event -> {
      ScaleTransition scaleTransition = new ScaleTransition(Duration.millis(200), signup);
      scaleTransition.setToX(1.1); 
      scaleTransition.setToY(1.1); 
      scaleTransition.play();
  });

  signup.setOnMouseExited(event -> {
      ScaleTransition scaleTransition = new ScaleTransition(Duration.millis(200), signup);
      scaleTransition.setToX(1.0); 
      scaleTransition.setToY(1.0); 
      scaleTransition.play();
  });

    Button login = new Button("Login");
    login.setFont(Font.font("Verdana", FontWeight.BOLD, 20));
    login.setStyle("-fx-background-color:WHITE;-fx-cursor: hand;");
    login.setPrefSize(120, 40);

    login.setOnAction(new EventHandler<ActionEvent>() {
      public void handle(ActionEvent event) {
        loginPage.showLoginScene();
      }
    });
     // Add mouse event handling for scaling effect
     login.setOnMouseEntered(event -> {
      ScaleTransition scaleTransition = new ScaleTransition(Duration.millis(200), login);
      scaleTransition.setToX(1.1); 
      scaleTransition.setToY(1.1); 
      scaleTransition.play();
  });

  login.setOnMouseExited(event -> {
      ScaleTransition scaleTransition = new ScaleTransition(Duration.millis(200), login);
      scaleTransition.setToX(1.0); 
      scaleTransition.setToY(1.0); 
      scaleTransition.play();
  });
    HBox hbcreateAcBox = new HBox(AcCreate);
    hbcreateAcBox.setAlignment(Pos.CENTER);

    HBox hbcreateInfo = new HBox(createInfo);
    hbcreateInfo.setAlignment(Pos.CENTER);

    Image glImage = new Image("com/example/Assets/Google.png");
    ImageView glIV = new ImageView(glImage);
    glIV.setFitHeight(35);
    glIV.setFitWidth(40);

    Image fbImage = new Image("com/example/Assets/Fb.png");
    ImageView fbIV = new ImageView(fbImage);
    fbIV.setFitHeight(35);
    fbIV.setFitWidth(40);

    Image lnImage = new Image("com/example/Assets/Linkdin.png");
    ImageView lnIV = new ImageView(lnImage);
    lnIV.setFitHeight(35);
    lnIV.setFitWidth(40);

    VBox gleBox = new VBox(glIV);
    VBox fbBox = new VBox(fbIV);
    VBox lnBox = new VBox(lnIV);

    HBox logo = new HBox(15, gleBox, fbBox, lnBox);
    logo.setPrefHeight(40);
    logo.setAlignment(Pos.CENTER);

    VBox vb1 = new VBox(hbcreateAcBox, logo, hbcreateInfo, userName, userEmail, userPassword);
    vb1.setSpacing(16);

    HBox signUpButton = new HBox(signup);
    signUpButton.setAlignment(Pos.CENTER);

    VBox vb = new VBox(30, vb1, signUpButton);
    vb.setAlignment(Pos.CENTER);
    vb.setPrefWidth(600);
    vb.setStyle(
        "-fx-padding: 50; -fx-background-color:WHITE;-fx-border-style: solid inside; -fx-border-width: 3; -fx-border-insets: 1; -fx-border-radius: 5; -fx-border-color:WHITE;");

    Label welbackLabel = new Label(" Welcome Back !");
    welbackLabel.setFont(Font.font("Noto Sans", FontWeight.BLACK, 40));
    welbackLabel.setStyle("-fx-text-fill:WHITE; -fx-font-weight: bold");

    Label sloganApp1 = new Label("To stay connected with us please ");
    sloganApp1.setFont(Font.font("Verdana", FontWeight.BOLD, 18));
    sloganApp1.setStyle("-fx-text-fill:WHITE;");

    Label sloganApp2 = new Label(" login with your personal information ");
    sloganApp2.setFont(Font.font("Verdana", FontWeight.BOLD, 18));
    sloganApp2.setStyle("-fx-text-fill:WHITE;");

    VBox info = new VBox(10, sloganApp1, sloganApp2);
    info.setAlignment(Pos.CENTER);

    VBox vbText = new VBox(50, welbackLabel, info, login);
    vbText.setAlignment(Pos.CENTER);
    vbText.setPrefWidth(400);
    vbText.setStyle("-fx-background-color: coral ");

    HBox hbAll = new HBox(0, vbText, vb);
    hbAll.setAlignment(Pos.CENTER);
    hbAll.setPrefSize(1000, 400);
    hbAll.setStyle(
        "-fx-border-insets: 1 ; -fx-border-width: 2 ; -fx-border-color:Beige;");

    Image brand = new Image("com/example/Assets/logo2.png");
    ImageView brandView = new ImageView(brand);
    brandView.setFitHeight(100);
    brandView.setFitWidth(100);
    brandView.setPreserveRatio(true);

    VBox None1 = new VBox(brandView);
    None1.setPrefHeight(150);
    None1.setAlignment(Pos.TOP_CENTER);
    VBox None2 = new VBox();
    None2.setPrefWidth(200);
    VBox None3 = new VBox();
    None3.setPrefHeight(150);
    VBox None4 = new VBox();
    None4.setPrefWidth(200);

    BorderPane bordPane = new BorderPane(hbAll, None1, None2, None3, None4);

    // Add rotation transition
    RotateTransition rotateTransition = new RotateTransition(Duration.seconds(10), logo);
    rotateTransition.setByAngle(360);
    rotateTransition.setCycleCount(RotateTransition.INDEFINITE);
    rotateTransition.setAutoReverse(false);
    rotateTransition.setAxis(Rotate.Y_AXIS);
    rotateTransition.play();

    RotateTransition rotateTransition2 = new RotateTransition(Duration.seconds(8), brandView);
    rotateTransition2.setByAngle(360);
    rotateTransition2.setCycleCount(RotateTransition.INDEFINITE);
    rotateTransition2.setAutoReverse(false);
    rotateTransition2.setAxis(Rotate.Y_AXIS);
    rotateTransition2.play();

    ScaleTransition scaleTransition = new ScaleTransition(Duration.seconds(1), hbAll);
    scaleTransition.setFromX(0.5);
    scaleTransition.setFromY(0.5);
    scaleTransition.setToX(1);
    scaleTransition.setToY(1);
    scaleTransition.play();

    ScaleTransition logoScaleTransition = new ScaleTransition(Duration.seconds(2), logo);
    logoScaleTransition.setFromX(0.5);
    logoScaleTransition.setFromY(0.5);
    logoScaleTransition.setToX(1);
    logoScaleTransition.setToY(1);
    logoScaleTransition.play();

    // // Add fade transition to pane
    FadeTransition fadeTransition = new FadeTransition(Duration.seconds(2), bordPane);
    fadeTransition.setFromValue(0);
    fadeTransition.setToValue(1);
    fadeTransition.play();

    Scene scene = new Scene(bordPane, 1400, 800);
    scene.setFill(Color.BEIGE);

    return scene;
  }

  // Method to handle signup action
  private void handleSignup(Stage prStage, String username, String password , String email) {
    DataService dataService; // Local instance of DataService
    try {
      dataService = new DataService(); // Initialize DataService instance
      // Create a map to hold user data
      Map<String, Object> data = new HashMap<>();
      data.put("password", password);
      data.put("username", username);
      data.put("emailid",email);

      // Add user data to Firestore
      dataService.addData("users", username, data);
      System.out.println("User registered successfully");

      // Navigate back to the login scene
      // loginPage.showLoginScene();
    } catch (Exception ex) {
      ex.printStackTrace();
    }
  }
  
}
