package com.example.Module2.Controller;

import javafx.animation.RotateTransition;
import javafx.animation.ScaleTransition;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.effect.BoxBlur;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.transform.Rotate;
import javafx.stage.Stage;
import javafx.util.Duration;

public class landingPage2 {

    private landingPage1 landingPage1;
    private Pane pane;
    private Scene scene;
    private Stage prStage;

    /*
     * Author : YASHODIP THAKARE
     * Desc : COnstructor of the class Landing Page 2 
     */
    public landingPage2(landingPage1 landingPage1) {
        this.landingPage1 = landingPage1;
    }

    /*
     * Author : YASHODIP THAKARE
     * Desc : It will return Scene of the landing Page 2 , it will be called by previous class (landingPage1).
     */
    public Scene callLandingPage2(Stage prStage) {
        
        this.prStage = prStage;
        Label logoName = new Label("4 W h e e l s");
        logoName.setFont(Font.font("Bodoni MT Condensed", FontWeight.LIGHT, 90));
        logoName.setTextFill(Color.LAVENDER);
        logoName.setOpacity(0.3);
        logoName.setScaleY(1.2);

        /*
     * Author : YASHODIP THAKARE
     * Desc : HBox contain logo name 
     */
        HBox logo = new HBox();
        logo.getChildren().add(logoName);
        logo.setLayoutX(560);
        logo.setLayoutY(320);
        logo.setPrefSize(600, 300);

        /*
     * Author : YASHODIP THAKARE
     * Desc : Navigation buttons Back Button and Next Button
     */
        Button next = new Button();
        next.setText("Next");
        next.setFont(Font.font("Arial", FontWeight.BOLD, 20));
        next.setStyle("-fx-background-color: coral ; -fx-text-fill: WHITE ; -fx-cursor : Hand ;");
        next.setPrefSize(120, 40);
        next.setLayoutX(680);
        next.setLayoutY(500);
        next.setOpacity(1);

        next.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent event) {
                handleLandingPage2();
            }
        });
         // Add mouse event handling for scaling effect
         next.setOnMouseEntered(event -> {
            ScaleTransition scaleTransition = new ScaleTransition(Duration.millis(200), next);
            scaleTransition.setToX(1.1); 
            scaleTransition.setToY(1.1); 
            scaleTransition.play();
        });

        next.setOnMouseExited(event -> {
            ScaleTransition scaleTransition = new ScaleTransition(Duration.millis(200), next);
            scaleTransition.setToX(1.0); 
            scaleTransition.setToY(1.0); 
            scaleTransition.play();
        });

        Button back = new Button();
        back.setText("Back");
        back.setFont(Font.font("Arial", FontWeight.BOLD, 20));
        back.setStyle("-fx-background-color: coral ; -fx-text-fill: WHITE ; -fx-cursor : Hand ;");
        back.setPrefSize(120, 40);
        back.setLayoutX(100);
        back.setLayoutY(500);
        back.setOpacity(1);

        back.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent event) {
                landingPage1.showLandingPage1scene();
            }
        });
         // Add mouse event handling for scaling effect
         back.setOnMouseEntered(event -> {
            ScaleTransition scaleTransition = new ScaleTransition(Duration.millis(200), back);
            scaleTransition.setToX(1.1); 
            scaleTransition.setToY(1.1); 
            scaleTransition.play();
        });

        back.setOnMouseExited(event -> {
            ScaleTransition scaleTransition = new ScaleTransition(Duration.millis(200), back);
            scaleTransition.setToX(1.0); 
            scaleTransition.setToY(1.0); 
            scaleTransition.play();
        });

        /*
     * Author : YASHODIP THAKARE
     * Desc : BoxBlur class makes images blurry 
     */
        BoxBlur bb = new BoxBlur();
        bb.setWidth(8);
        bb.setHeight(8);
        bb.setIterations(10);

        Image image = new Image("com/example/Assets/runCar.jpg");
        ImageView page1 = new ImageView(image);
        page1.setFitWidth(1400);
        page1.setFitHeight(1000);
        page1.setEffect(bb);

          /*
     * Author : YASHODIP THAKARE
     * Desc : It contains Back And next Button
     */
        HBox navigateButton = new HBox(1100);
        navigateButton.getChildren().addAll(back,next);     

        // Hero Section
        Text headline = new Text("Your Ultimate Car Buying Guide");
        headline.setFont(Font.font("Arial", FontWeight.BOLD, 36));
        headline.setFill(Color.BEIGE);
        
        Text subheadline = new Text("Detailed reviews, comparisons, and the best deals on new and used cars.");
        subheadline.setFont(Font.font("Arial", FontWeight.NORMAL, 20));
        subheadline.setFill(Color.BEIGE);

          /*
     * Author : YASHODIP THAKARE
     * Desc : It contain the headline and subheadline text
     */
        VBox heroSection = new VBox(headline, subheadline);
        heroSection.setSpacing(10);
        heroSection.setStyle("-fx-alignment: center;");

          /*
     * Author : YASHODIP THAKARE
     * Desc : Informative statements using Text class
     */
        Text newCarsHeader = new Text("New Cars");
        newCarsHeader.setFont(Font.font("Arial", FontWeight.BOLD, 24));
        newCarsHeader.setFill(Color.BEIGE);

        Text newCarsText = new Text("Discover the latest models with comprehensive reviews and ratings. Find the perfect new car to fit your lifestyle and budget.");
        newCarsText.setFont(Font.font("Arial", FontWeight.NORMAL, 16));
        newCarsText.setFill(Color.BEIGE);

        Text usedCarsHeader = new Text("Used Cars");
        usedCarsHeader.setFont(Font.font("Arial", FontWeight.BOLD, 24));
        usedCarsHeader.setFill(Color.BEIGE);

        Text usedCarsText = new Text("Explore a wide range of verified used cars. Enjoy the benefits of lower prices with the assurance of quality and reliability.");
        usedCarsText.setFont(Font.font("Arial", FontWeight.NORMAL, 16));
        usedCarsText.setFill(Color.BEIGE);

        Text compareCarsHeader = new Text("Compare Cars");
        compareCarsHeader.setFont(Font.font("Arial", FontWeight.BOLD, 24));
        compareCarsHeader.setFill(Color.BEIGE);

        Text compareCarsText = new Text("Easily compare specifications, prices, and features of different car models. Make an informed decision with our advanced comparison tools.");
        compareCarsText.setFont(Font.font("Arial", FontWeight.NORMAL, 16));
        compareCarsText.setFill(Color.BEIGE);

        Text customerReviewsHeader = new Text("Customer Reviews");
        customerReviewsHeader.setFont(Font.font("Arial", FontWeight.BOLD, 24));
        customerReviewsHeader.setFill(Color.BEIGE);

        Text customerReviewsText = new Text("Read honest reviews from real customers. Learn from their experiences to make the best choice for your next car.");
        customerReviewsText.setFont(Font.font("Arial", FontWeight.NORMAL, 16));
        customerReviewsText.setFill(Color.BEIGE);
       
         /*
     * Author : YASHODIP THAKARE
     * Desc : It contain texts  
     */
        VBox infoSections = new VBox(20);
        infoSections.setStyle("-fx-padding: 20; -fx-border-radius: 10;");
        infoSections.setOpacity(1);
        infoSections.getChildren().addAll(newCarsHeader, newCarsText, usedCarsHeader, usedCarsText, compareCarsHeader, compareCarsText, customerReviewsHeader, customerReviewsText);

        Text trustHeader = new Text("Why Choose Us?");
        trustHeader.setFont(Font.font("Arial", FontWeight.BOLD, 24));
        trustHeader.setFill(Color.BEIGE);

        Text trustedLogo = new Text("* 100% Trusted");
        trustedLogo.setFont(Font.font("Arial", FontWeight.BOLD, 20));
        trustedLogo.setFill(Color.BEIGE);

        Text bestPriceLogo = new Text("* Best Price Guarantee");
        bestPriceLogo.setFont(Font.font("Arial", FontWeight.BOLD, 20));
        bestPriceLogo.setFill(Color.BEIGE);
        
        /*
     * Author : YASHODIP THAKARE
     * Desc : It has the trustedlogo and bestPricelogo
     */
        HBox logos = new HBox();
        logos.setSpacing(50);
        logos.setStyle("-fx-alignment: center;");
        logos.getChildren().addAll(trustedLogo, bestPriceLogo);

        Text socialProofHeader = new Text("What Our Customers Say");
        socialProofHeader.setFont(Font.font("Arial", FontWeight.BOLD, 24));
        socialProofHeader.setFill(Color.BEIGE);

        Text testimonial1 = new Text("\"I found my dream car effortlessly on this site. The process was smooth and transparent!\" - Yashodip Thakare.");
        testimonial1.setFont(Font.font("Arial", FontWeight.NORMAL, 16));
        testimonial1.setFill(Color.BEIGE);
        
        Text testimonial2 = new Text("\"Excellent service and a wide variety of cars to choose from. Highly recommend!\" - Vaibhav Patil.");
        testimonial2.setFont(Font.font("Arial", FontWeight.NORMAL, 16));
        testimonial2.setFill(Color.BEIGE);
      
         /*
     * Author : YASHODIP THAKARE
     * Desc : Trust Section
     */
        VBox trustSection = new VBox();
        trustSection.setSpacing(20);
        trustSection.setStyle("-fx-alignment: center;");
        trustSection.getChildren().addAll(trustHeader, logos);
 /*
     * Author : YASHODIP THAKARE
     * Desc : Review Section
     */
        VBox socialProofSection = new VBox();
        socialProofSection.setSpacing(10);
        socialProofSection.setStyle("-fx-alignment: center;");
        socialProofSection.getChildren().addAll(socialProofHeader, testimonial1, testimonial2);
        

        VBox mainLayout = new VBox(40);
        mainLayout.setStyle("-fx-padding: 20;-fx-background-size: cover;");
        mainLayout.setLayoutX(150);
        mainLayout.setLayoutY(50);

         /*
     * Author : YASHODIP THAKARE
     * Desc : All the sections to the main Layout
     */
        mainLayout.getChildren().addAll(heroSection, infoSections, trustSection, socialProofSection);
        mainLayout.setAlignment(Pos.CENTER);
        mainLayout.setStyle("-fx-padding: 20; -fx-border-radius: 10;");

         /*
     * Author : YASHODIP THAKARE
     * Desc : Adding image Brand 
     */
        Image brand = new Image("com/example/Assets/logo2.png");
        ImageView brandView = new ImageView(brand);
        brandView.setFitHeight(100);
        brandView.setFitWidth(100);
        brandView.setLayoutX(50);
        brandView.setLayoutY(20);
        brandView.setPreserveRatio(true);

         /*
     * Author : YASHODIP THAKARE
     * Desc : Animation to the brand logo
     */
        RotateTransition rotateTransition2 = new RotateTransition(Duration.seconds(15), brandView);
        rotateTransition2.setByAngle(360);
        rotateTransition2.setCycleCount(RotateTransition.INDEFINITE);
        rotateTransition2.setAutoReverse(false);
        rotateTransition2.setAxis(Rotate.Y_AXIS);
        rotateTransition2.play();

         /*
     * Author : YASHODIP THAKARE
     * Desc : StackPane used over the footer line to show the Navigating Buttons 
     */
        StackPane navigateXreview = new StackPane(socialProofSection,navigateButton);
        navigateXreview.setLayoutY(700);
        navigateXreview.setLayoutX(25);
        
         /*
     * Author : YASHODIP THAKARE
     * Desc : It has all the data of the page 
     */
        pane = new Pane( page1,logo,mainLayout,navigateXreview , brandView);

        scene = new Scene(pane, 1400, 800);
        return scene;
    }

      /*
     * Author : YASHODIP THAKARE
     * Desc : The method returns the landingPage2 Scene
     */
    public void showLandingPage2scene() {
        prStage.setScene(scene);
        prStage.show();
    }

      /*
     * Author : YASHODIP THAKARE
     * Desc : After clicking next Button , the method is being called.
     */
    public void handleLandingPage2() {
        if (prStage != null) {
            loginPage controllLoginPage = new loginPage(this);
            Scene loginscene = controllLoginPage.initLoginScene(prStage);
            prStage.setScene(loginscene);
            prStage.getIcons().add(new Image("com/example/Assets/logo2.png"));
            prStage.show();
        }

    }

}
