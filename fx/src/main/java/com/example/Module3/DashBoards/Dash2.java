package com.example.Module3.DashBoards;

import com.example.Module2.Controller.loginPage;

import javafx.animation.RotateTransition;
import javafx.animation.ScaleTransition;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.transform.Rotate;
import javafx.stage.Stage;
import javafx.util.Duration;

/**
 * Dash2 Class
 * 
 * This class represents a user interface for displaying car information in a dashboard format. 
 * It provides functionality to display car images, navigate between different scenes, and handle user interactions.
 * 
 * Author: Yashodip Thakare
 */

 public class Dash2 {
    // Constants for UI element sizes
    private static final int Number = 48;
    private static final int ImageWidth = 325;
    private static final int ImageHeight = 170;
    private static final int VBOXWidth = 325;
    private static final int VBOXHeight = 320;
    
    // References to other classes
    private DashBoard dashBoard;
    private static Stage prStage;
    private loginPage loginPage;
    private static Scene scene;
    private Dash3 dash3; 

    /**
     * Constructor for Dash2
     * 
     * @param dashBoard Reference to the DashBoard instance
     * @param loginPage Reference to the loginPage instance
     */
    public Dash2(DashBoard dashBoard, loginPage loginPage) {
        this.dashBoard = dashBoard;
        this.loginPage = loginPage;
    }

    /**Author : Yashodip Thakare
     * Method to initialize and display the Dash2 scene
     * 
     * @param prStage The primary stage where the scene will be displayed
     * @return The constructed Scene object
     */
    public Scene callDash2(Stage prStage) {
        this.prStage = prStage;

        // Initialize the grid layout
        GridPane grid = new GridPane();
        grid.setPadding(new Insets(20, 50, 20, 0));
        grid.setVgap(20);
        grid.setHgap(15);
        grid.setAlignment(Pos.CENTER);
        
        /*Author : Yashodip Thakare
        * Array of car names
        */        
        String str[] = { "EXTER        6-10Lac","MERCEDES       66Lac","SELTOS       10-20Lac","SONET       8-15Lac","CITROEN        7-9Lac","KUSHAQ        10-18Lac","SLAVIA      10-18lac","ALTROZ_RACER       9-11Lac","CELERIO      5-7Lac","ALTO-K10     4-6Lac","S-PRESSO       4-6Lac","MG-GLOSTER     38-43Lac","MERIDIAN     33-40Lac","XUV700      13-27Lac","MAGNITE       6-11LAc","BMW_X3       68-88Lac","FRONX      7-13Lac",
    "EXTER        6-10Lac","MERCEDES       66Lac","SELTOS       10-20Lac","SONET       8-15Lac","CITROEN        7-9Lac","KUSHAQ        10-18Lac","SLAVIA      10-18lac","ALTROZ_RACER       9-11Lac","CELERIO      5-7Lac","ALTO-K10     4-6Lac","S-PRESSO       4-6Lac","MG-GLOSTER     38-43Lac","MERIDIAN     33-40Lac","XUV700      13-27Lac","MAGNITE       6-11LAc","BMW_X3       68-88Lac","FRONX      7-13Lac",
"EXTER        6-10Lac","MERCEDES       66Lac","SELTOS       10-20Lac","SONET       8-15Lac","CITROEN        7-9Lac","KUSHAQ        10-18Lac","SLAVIA      10-18lac","ALTROZ_RACER       9-11Lac","CELERIO      5-7Lac","ALTO-K10     4-6Lac","S-PRESSO       4-6Lac","MG-GLOSTER     38-43Lac","MERIDIAN     33-40Lac","XUV700      13-27Lac","MAGNITE       6-11LAc","BMW_X3       68-88Lac","FRONX      7-13Lac"};
        
        /*Author : Yashodip Thakare 
            Array of image file names
        */  
        String dashCars[] = {"DashCar0", "DashCar1","DashCar2","DashCar3","DashCar4","DashCar5","DashCar6","DashCar7","DashCar8","DashCar9","DashCar10","DashCar11","DashCar12","DashCar13","DashCar14","DashCar15",
    "DashCar0", "DashCar1","DashCar2","DashCar3","DashCar4","DashCar5","DashCar6","DashCar7","DashCar8","DashCar9","DashCar10","DashCar11","DashCar12","DashCar13","DashCar14","DashCar15",
"DashCar0", "DashCar1","DashCar2","DashCar3","DashCar4","DashCar5","DashCar6","DashCar7","DashCar8","DashCar9","DashCar10","DashCar11","DashCar12","DashCar13","DashCar14","DashCar15"};

        /*Author : Yashodip Thakare
         Loop to create car display elements
        */
         
        for (int i = 0; i < Number; i++) {
            VBox vbox = new VBox();
            vbox.setPrefSize(VBOXWidth, VBOXHeight);
            vbox.setStyle("-fx-border-insets: 2; -fx-border-width: 5 ; -fx-border-color: Black;");

            Label lb1 = new Label();
            lb1.setText(str[i]);
            lb1.setStyle("-fx-font-size: 18px ; -fx-font-weight: bold ;");

            Button button = new Button();
            Image click = new Image("com/example/Assets/click.png");
            ImageView clImageView = new ImageView(click);
            clImageView.setFitHeight(40);
            clImageView.setFitWidth(50);
            button.setGraphic(clImageView);
            button.setStyle("-fx-font-size: 14px ; -fx-font-weight: bold ; -fx-cursor : hand ; -fx-background-color: Transparent");
            button.setOnAction(event -> {
                System.out.println("Button Clicked");
                handleCarInfo();
            });

            HBox vb = new HBox(80, button);
            vb.setPrefHeight(70);
            vb.setAlignment(Pos.CENTER);
            vb.setStyle(" -fx-padding: 12px");

            Image image = new Image("com/example/Assets/" + dashCars[i] + ".jpeg");
            ImageView imageView = new ImageView(image);
            imageView.setFitWidth(ImageWidth);
            imageView.setFitHeight(ImageHeight);

            Label lb = new Label();
            lb.setText(str[i]);
            lb.setFont(Font.font("Verdana", FontWeight.BOLD, 18));

            vbox.getChildren().addAll(imageView, lb, vb);
            vbox.setStyle("-fx-padding: 5px ; -fx-background-color : WHITE ;");

             // Add mouse event handling for scaling effect
            vbox.setOnMouseEntered(event -> {
                ScaleTransition scaleTransition = new ScaleTransition(Duration.millis(200), vbox);
                scaleTransition.setToX(1.1); 
                scaleTransition.setToY(1.1); 
                scaleTransition.play();
            });

            vbox.setOnMouseExited(event -> {
                ScaleTransition scaleTransition = new ScaleTransition(Duration.millis(200), vbox);
                scaleTransition.setToX(1.0); 
                scaleTransition.setToY(1.0); 
                scaleTransition.play();
            });

            /*
             * Author : Yashodip Thakare
             * calculations used for the GridPane Table 
             */
            int col = i % 4 + 1;
            int row = i / 4 + 1;

            grid.add(vbox, col, row);
            grid.setStyle("-fx-background-color: GHOSTWHITE;");
        }

        /*
        Author : Yashodip Thakare 
         Create and animate the logo
        */
         Image logo = new Image("com/example/Assets/logo2.png");
        ImageView logoImageView = new ImageView(logo);
        logoImageView.setFitWidth(80);
        logoImageView.setFitHeight(80);

        RotateTransition rotateTransition2 = new RotateTransition(Duration.seconds(15), logoImageView);
        rotateTransition2.setByAngle(360);
        rotateTransition2.setCycleCount(RotateTransition.INDEFINITE);
        rotateTransition2.setAutoReverse(false);
        rotateTransition2.setAxis(Rotate.Y_AXIS);
        rotateTransition2.play();

        /* Author : Yashodip Thakare
             Create the Home button
         */
        Button Home = new Button();
        Home.setText("Home");
        DropShadow ds = new DropShadow();
        ds.setOffsetY(3.0f);
        ds.setColor(Color.color(0.4f, 0.4f, 0.4f));

        Home.setPrefSize(180, 55);
        Home.setStyle("-fx-background-color: Beige; -fx-text-fill: Black ; -fx-cursor: hand ;");
        Home.setFont(Font.font("Verdana", FontWeight.EXTRA_BOLD, 18));
        Home.setOnAction(event -> {
            DashBoard.showDashBoardscene();
            System.out.println("clicked");
            Home.setEffect(ds);
        });

        /*Author : Yashodip Thakare
                Create the PopularCars button
        */
        Button PopularCars = new Button();
        PopularCars.setText("Popular Cars");
        PopularCars.setPrefSize(180, 55);
        PopularCars.setStyle("-fx-background-color: Beige; -fx-text-fill: Black ; -fx-cursor: hand");
        PopularCars.setFont(Font.font("Verdana", FontWeight.EXTRA_BOLD, 18));
        PopularCars.setEffect(ds);

        /* Author : Yashodip Thakare
         Create the TrendingCars button
         */
        Button TrendingCars = new Button();
        TrendingCars.setText("Trending Cars");
        TrendingCars.setPrefSize(180, 55);
        TrendingCars.setStyle("-fx-background-color: Beige; -fx-text-fill: Black ; -fx-cursor : hand");
        TrendingCars.setFont(Font.font("Verdana", FontWeight.EXTRA_BOLD, 18));
        TrendingCars.setOnAction(event -> {
            handleDash3();
            TrendingCars.setEffect(ds);
        });
        /*
        Author : Yashodip Thakare 
                 Header buttons layout
        */
        HBox headbuttons = new HBox(logoImageView, Home, PopularCars, TrendingCars);
        headbuttons.setSpacing(30);
        headbuttons.setPrefWidth(800);
        headbuttons.setAlignment(Pos.CENTER);

        /* Author : Yashodip Thakare
            Logout and Location 
        */
        Button location = new Button();
        location.setPrefSize(35, 35);
        Image locimage = new Image("com/example/Assets/pinLoc.png");
        ImageView locImageView = new ImageView(locimage);
        locImageView.setFitHeight(35);
        locImageView.setFitWidth(35);
        location.setGraphic(locImageView);
        location.setStyle("-fx-background-color: Beige; -fx-text-fill: Black ; -fx-cursor : hand ;");
        location.setFont(Font.font("Verdana", FontWeight.BOLD, 20));
        location.setOnAction(event -> {
            SearchLocation.showLocationSearch();
        });

        Button logout = new Button();
        logout.setText("Logout");
        logout.setPrefSize(150, 55);
        logout.setStyle("-fx-background-color: Beige; -fx-text-fill: Black ; -fx-cursor : hand");
        logout.setFont(Font.font("Verdana", FontWeight.BOLD, 20));
        logout.setOnAction(event -> {
            loginPage.handleLogout();
            logout.setEffect(ds);
        });

        HBox headRightBox = new HBox(location, logout);
        headRightBox.setSpacing(20);
        headRightBox.setAlignment(Pos.CENTER);

        // Top header layout
        HBox TopHeader = new HBox(300, headbuttons, headRightBox);
        TopHeader.setPrefSize(1400, 100);
        TopHeader.setStyle("-fx-background-color : Beige");
        TopHeader.setAlignment(Pos.CENTER);

        // Main content layout
        HBox hb = new HBox(grid);
        hb.setLayoutY(100);
        hb.setPrefHeight(700);
        hb.setPrefWidth(1400);

        // Scroll pane for main content
        ScrollPane sc = new ScrollPane(hb);
        sc.setVbarPolicy(ScrollPane.ScrollBarPolicy.ALWAYS);
        sc.setPrefSize(1400, 700);
        sc.setLayoutY(100);

        // Root pane layout
        Pane pane = new Pane();
        pane.getChildren().addAll(TopHeader, sc);

        // Construct and return the scene
        scene = new Scene(pane, 1400, 800);
        return scene;
    }

    /*Author : Yashodip Thakare
     * Method to display the Dash2 scene
     */
    public static void showDash2scene() {
        prStage.setScene(scene);
        prStage.getIcons().add(new Image("com/example/Assets/logo2.png"));
        prStage.show();
    }

    /*Author : Yashodip Thakare
     * Method to display the Dash2 scene
     */
    public void handleDash3() {
        Dash3 controllDash3 = new Dash3(dashBoard, this, loginPage);
        Scene dash3Scene = controllDash3.callDash3(prStage);
        prStage.setScene(dash3Scene);
        prStage.show();
    }

    /*Author : Yashodip Thakare
     * Method to display the Dash2 scene
     */
    public void handleCarInfo() {
        carInfo controllCarInfo = new carInfo(dashBoard, this, dash3);
        Scene carinfoScene = controllCarInfo.callCarInfo(prStage);
        prStage.setScene(carinfoScene);
        prStage.show();
    }
}

// public void start(Stage prStage) throws Exception {

// VBox hh1 = new VBox();
// hh1.setPrefHeight(70);

// VBox hh2 = new VBox();
// hh2.setPrefHeight(70);

// VBox hh3 = new VBox();
// hh3.setPrefHeight(70);

// VBox hh4 = new VBox();
// hh4.setPrefHeight(70);

// VBox hh5 = new VBox();
// hh5.setPrefHeight(70);

// VBox hh6 = new VBox();
// hh6.setPrefHeight(70);

// VBox hh7 = new VBox();
// hh7.setPrefHeight(70);

// VBox hh8 = new VBox();
// hh8.setPrefHeight(70);

// Image image1 = new Image("com/example/Assets/NatureCar.jpg");
// ImageView imageView1 = new ImageView(image1);
// imageView1.setFitHeight(180);
// imageView1.setFitWidth(325);

// Label lb1 = new Label("BMW");
// lb1.setStyle("-fx-text-fill: RED; -fx-cursor : Hand ;");

// Image image2 = new Image("com/example/Assets/NatureCar.jpg");
// ImageView imageView2 = new ImageView(image2);
// imageView2.setFitHeight(180);
// imageView2.setFitWidth(325);

// Image image3 = new Image("com/example/Assets/NatureCar.jpg");
// ImageView imageView3 = new ImageView(image3);
// imageView3.setFitHeight(180);
// imageView3.setFitWidth(325);

// Image image4 = new Image("com/example/Assets/NatureCar.jpg");
// ImageView imageView4 = new ImageView(image4);
// imageView4.setFitHeight(180);
// imageView4.setFitWidth(325);

// Image image5 = new Image("com/example/Assets/NatureCar.jpg");
// ImageView imageView5 = new ImageView(image5);
// imageView5.setFitHeight(180);
// imageView5.setFitWidth(325);

// Image image6 = new Image("com/example/Assets/NatureCar.jpg");
// ImageView imageView6 = new ImageView(image6);
// imageView6.setFitHeight(180);
// imageView6.setFitWidth(325);

// Image image7 = new Image("com/example/Assets/NatureCar.jpg");
// ImageView imageView7 = new ImageView(image7);
// imageView7.setFitHeight(180);
// imageView7.setFitWidth(325);

// Image image8 = new Image("com/example/Assets/NatureCar.jpg");
// ImageView imageView8 = new ImageView(image8);
// imageView8.setFitHeight(180);
// imageView8.setFitWidth(325);

// VBox vbox_1 = new VBox(hh1,imageView1, lb1);
// vbox_1.setPrefSize(260, 320);
// vbox_1.setStyle("-fx-background-color: RED; -fx-cursor : Hand ;");

// VBox vbox_2 = new VBox(hh2,imageView2);
// vbox_2.setPrefSize(325, 320);

// VBox vbox_3 = new VBox(hh3,imageView3);
// vbox_3.setPrefSize(325, 320);

// VBox vbox_4 = new VBox(hh4,imageView4);
// vbox_4.setPrefSize(325, 320);

// VBox vbox_5 = new VBox(hh5,imageView5);
// vbox_5.setPrefSize(325, 320);

// VBox vbox_6 = new VBox(hh6,imageView6);
// vbox_6.setPrefSize(325, 320);

// VBox vbox_7 = new VBox(hh7,imageView7);
// vbox_7.setPrefSize(325, 320);

// VBox vbox_8 = new VBox(hh8,imageView8);
// vbox_8.setMaxSize(325, 320);

// GridPane gridPane = new GridPane();
// gridPane.add(vbox_1, 1, 1);
// gridPane.add(vbox_2, 2, 1);
// gridPane.add(vbox_3, 3, 1);
// gridPane.add(vbox_4, 4, 1);
// gridPane.add(vbox_5, 1, 2);
// gridPane.add(vbox_6, 2, 2);
// gridPane.add(vbox_7, 3, 2);
// gridPane.add(vbox_8, 4, 2);

// gridPane.setHgap(15);
// gridPane.setVgap(20);

// // gridPane.setPrefSize(1300,700);

// gridPane.setPadding(new Insets(20, 35, 20, 5));
// gridPane.setAlignment(Pos.CENTER);

// Pane paneScrollPane = new Pane(gridPane);

// ScrollPane sc = new ScrollPane(paneScrollPane);
// sc.setVbarPolicy(ScrollPane.ScrollBarPolicy.ALWAYS);
// sc.setPrefSize(1400, 700);
// sc.setLayoutY(100);

// Pane paneMain = new Pane(sc);

// Scene scene = new Scene(paneMain, 1400, 800);

// prStage.setScene(scene);
// prStage.setTitle("4Wheels");
// prStage.show();
// }
