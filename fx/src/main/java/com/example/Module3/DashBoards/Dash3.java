package com.example.Module3.DashBoards;

import com.example.Module2.Controller.loginPage;

import javafx.animation.RotateTransition;
import javafx.animation.ScaleTransition;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.transform.Rotate;
import javafx.stage.Stage;
import javafx.util.Duration;

public class Dash3 {
    private static final int Number = 48;
    private static final int ImageWidth = 325;
    private static final int ImageHeight = 170;
    private static final int VBOXWidth = 325;
    private static final int VBOXHeight = 320;
    private DashBoard dashBoard;
    private Dash2 dash2;
    private static Stage prStage;
    private static Scene scene;
    private loginPage loginPage;

    /**
     * AuthorName : Yashodip Thakare
     * Desc: This is the constructor of the Dash3 class, which initializes the
     * object with three parameters: dashBoard, dash2, and loginPage.
     */
    public Dash3(DashBoard dashBoard, Dash2 dash2, loginPage loginPage) {
        this.dashBoard = dashBoard;
        this.dash2 = dash2;
        this.loginPage = loginPage;
    }

    /**
     * AuthorName : Yashodip Thakare
     * Desc: This method creates a new Scene for the Dash3 stage. It takes a Stage
     * object as a parameter and returns a Scene object.
     */
    public Scene callDash3(Stage prStage) {
        this.prStage = prStage;

        /**
         * AuthorName : Yashodip Thakare
         * Desc: This code creates a new GridPane and configures its padding, vertical
         * gap, horizontal gap, and alignment.
         */
        GridPane grid = new GridPane();
        grid.setPadding(new Insets(20, 50, 20, 0));
        grid.setVgap(20);
        grid.setHgap(15);
        grid.setAlignment(Pos.CENTER);

        /**
         * AuthorName : Yashodip Thakare
         * Desc: These two string arrays store the names of cars and their corresponding
         * dash car names.
         */
        String str[] = { "FRONX      7-13Lac", "BMW_X3       68-88Lac", "MAGNITE       6-11LAc", "XUV700      13-27Lac",
                "MERIDIAN     33-40Lac", "MG-GLOSTER     38-43Lac", "S-PRESSO       4-6Lac", "ALTO-K10     4-6Lac",
                "CELERIO      5-7Lac", "ALTROZ_RACER       9-11Lac", "SLAVIA      10-18lac", "KUSHAQ        10-18Lac",
                "CITROEN        7-9Lac", "SONET       8-15Lac", "SELTOS       10-20Lac", "MERCEDES       66Lac",
                "EXTER        6-10Lac",
                "EXTER        6-10Lac", "MERCEDES       66Lac", "SELTOS       10-20Lac", "SONET       8-15Lac",
                "CITROEN        7-9Lac", "KUSHAQ        10-18Lac", "SLAVIA      10-18lac", "ALTROZ_RACER       9-11Lac",
                "CELERIO      5-7Lac", "ALTO-K10     4-6Lac", "S-PRESSO       4-6Lac", "MG-GLOSTER     38-43Lac",
                "MERIDIAN     33-40Lac", "XUV700      13-27Lac", "MAGNITE       6-11LAc", "BMW_X3       68-88Lac",
                "FRONX      7-13Lac",
                "EXTER        6-10Lac", "MERCEDES       66Lac", "SELTOS       10-20Lac", "SONET       8-15Lac",
                "CITROEN        7-9Lac", "KUSHAQ        10-18Lac", "SLAVIA      10-18lac", "ALTROZ_RACER       9-11Lac",
                "CELERIO      5-7Lac", "ALTO-K10     4-6Lac", "S-PRESSO       4-6Lac", "MG-GLOSTER     38-43Lac",
                "MERIDIAN     33-40Lac", "XUV700      13-27Lac", "MAGNITE       6-11LAc", "BMW_X3       68-88Lac",
                "FRONX      7-13Lac" };

        String dashCars[] = { "DashCar15", "DashCar14", "DashCar13", "DashCar12", "DashCar11", "DashCar10", "DashCar9",
                "DashCar8", "DashCar7", "DashCar6", "DashCar5", "DashCar4", "DashCar3", "DashCar2", "DashCar1",
                "DashCar0",
                "DashCar0", "DashCar1", "DashCar2", "DashCar3", "DashCar4", "DashCar5", "DashCar6", "DashCar7",
                "DashCar8", "DashCar9", "DashCar10", "DashCar11", "DashCar12", "DashCar13", "DashCar14", "DashCar15",
                "DashCar0", "DashCar1", "DashCar2", "DashCar3", "DashCar4", "DashCar5", "DashCar6", "DashCar7",
                "DashCar8", "DashCar9", "DashCar10", "DashCar11", "DashCar12", "DashCar13", "DashCar14", "DashCar15" };

        for (int i = 0; i < Number; i++) {
            /**
             * AuthorName : Yashodip Thakare
             * Desc: This code creates a VBox and a Button, and configures their styles and
             * properties. The button has an image and an action event handler that prints
             * "Button Clicked" and calls the handleCarInfo() method.
             */
            VBox vbox = new VBox();
            vbox.setPrefSize(VBOXWidth, VBOXHeight);
            vbox.setStyle("-fx-border-insets: 10 ; -fx-border-width: 5 ; -fx-border-color: Black;");

            Label lb1 = new Label();
            lb1.setText(str[i]);
            lb1.setStyle("-fx-font-size: 18px ; -fx-font-weight: bold ;");

            Button button = new Button();
            Image click = new Image("com/example/Assets/click.png");
            ImageView clImageView = new ImageView(click);
            clImageView.setFitHeight(40);
            clImageView.setFitWidth(50);
            button.setGraphic(clImageView);
            button.setStyle(
                    "-fx-font-size: 14px ; -fx-font-weight: bold ; -fx-cursor : hand ; -fx-background-color: Transparent");
            button.setOnAction(event -> {
                System.out.println("Button Clicked");
                handleCarInfo();
            });

            /**
             * AuthorName : Yashodip Thakare
             * Desc: This code creates an HBox and an ImageView, and configures their styles
             * and properties. The ImageView displays an image from the Assets folder.
             */
            HBox vb = new HBox(50, button);
            vb.setPrefHeight(70);
            vb.setAlignment(Pos.CENTER);
            vb.setStyle(" -fx-padding: 5px");

            Image image = new Image("com/example/Assets/" + dashCars[i] + ".jpeg");
            ImageView imageView = new ImageView(image);
            imageView.setFitWidth(ImageWidth);
            imageView.setFitHeight(ImageHeight);

            Label lb = new Label();
            lb.setText(str[i]);
            lb.setFont(Font.font("Verdana", FontWeight.BOLD, 18));

            vbox.getChildren().addAll(imageView, lb, vb);
            vbox.setStyle("-fx-padding: 5px ; -fx-background-color : WHITE ;");
              // Add mouse event handling for scaling effect
            vbox.setOnMouseEntered(event -> {
                ScaleTransition scaleTransition = new ScaleTransition(Duration.millis(200), vbox);
                scaleTransition.setToX(1.1); 
                scaleTransition.setToY(1.1); 
                scaleTransition.play();
            });

            vbox.setOnMouseExited(event -> {
                ScaleTransition scaleTransition = new ScaleTransition(Duration.millis(200), vbox);
                scaleTransition.setToX(1.0); 
                scaleTransition.setToY(1.0); 
                scaleTransition.play();
            });

            int col = i % 4 + 1;
            int row = i / 4 + 1;

            /**
             * AuthorName : Yashodip Thakare
             * Desc: This code adds the VBox to the GridPane at the specified column and
             * row, and sets the GridPane's background color to GHOSTWHITE.
             */
            grid.add(vbox, col, row);
            grid.setStyle("-fx-background-color: GHOSTWHITE;");
        }

        // Author: Yashodip Thakare
        // Desc: Creating an Image object for the logo
        Image logo = new Image("com/example/Assets/logo2.png");

        // Author: Yashodip Thakare
        // Desc: Creating an ImageView object for the logo
        ImageView logoImageView = new ImageView(logo);
        logoImageView.setFitWidth(80);
        logoImageView.setFitHeight(80);

        // Author: Yashodip Thakare
        // Desc: Creating a RotateTransition object to rotate the logo
        RotateTransition rotateTransition2 = new RotateTransition(Duration.seconds(15), logoImageView);
        rotateTransition2.setByAngle(360);
        rotateTransition2.setCycleCount(RotateTransition.INDEFINITE);
        rotateTransition2.setAutoReverse(false);
        rotateTransition2.setAxis(Rotate.Y_AXIS);
        rotateTransition2.play();

        // Author: Yashodip Thakare
        // Desc: Creating a Button object for the "Home" button
        Button Home = new Button();//
        Home.setText("Home");

        // Author: Yashodip Thakare
        // Desc: Creating a DropShadow object for the button effects
        DropShadow ds = new DropShadow();
        ds.setOffsetY(3.0f);
        ds.setColor(Color.color(0.4f, 0.4f, 0.4f));

        // Author: Yashodip Thakare
        // Desc: Setting the properties of the "Home" button
        Home.setPrefSize(180, 55);
        Home.setStyle("-fx-background-color: Beige; -fx-text-fill: Black ; -fx-cursor: hand ;");
        Home.setFont(Font.font("Verdana", FontWeight.EXTRA_BOLD, 18));
        Home.setOnAction(event -> { // Lambda Function override the functional interface consist only one Abstract
                                    // class
            DashBoard.showDashBoardscene();
            Home.setEffect(ds);
        });

        // Author: Yashodip Thakare
        // Desc: Creating a Button object for the "Popular Cars" button
        Button PopularCars = new Button();
        PopularCars.setText("Popular Cars");
        PopularCars.setPrefSize(180, 55);
        PopularCars.setStyle("-fx-background-color: Beige; -fx-text-fill: Black ; -fx-cursor: hand");
        PopularCars.setFont(Font.font("Verdana", FontWeight.EXTRA_BOLD, 18));
        PopularCars.setOnAction(event -> { // Lambda Function override the functional interface consist only one
                                           // Abstract class
            Dash2.showDash2scene();
            PopularCars.setEffect(ds);
        });

        // Author: Yashodip Thakare
        // Desc: Creating a Button object for the "Trending Cars" button
        Button TrendingCars = new Button();
        TrendingCars.setText("Trending Cars");
        TrendingCars.setPrefSize(180, 55);
        TrendingCars.setStyle("-fx-background-color: Beige; -fx-text-fill: Black ; -fx-cursor : hand");
        TrendingCars.setFont(Font.font("Verdana", FontWeight.EXTRA_BOLD, 18));
        TrendingCars.setEffect(ds);
        TrendingCars.setOnAction(event -> { // Lambda Function override the functional interface consist only one
                                            // Abstract class

        });

        // Author: Yashodip Thakare
        // Desc: Creating an HBox object to hold the logo and buttons
        HBox headbuttons = new HBox(logoImageView, Home, PopularCars, TrendingCars);
        headbuttons.setSpacing(30);
        headbuttons.setPrefWidth(800);
        headbuttons.setAlignment(Pos.CENTER);

        // Author: Yashodip Thakare
        // Desc: Creating a Button object for the "Location" button
        Button location = new Button();
        location.setPrefSize(35, 35);
        Image locimage = new Image("com/example/Assets/pinLoc.png");
        ImageView locImageView = new ImageView(locimage);
        locImageView.setFitHeight(35);
        locImageView.setFitWidth(35);
        location.setGraphic(locImageView);
        location.setStyle("-fx-background-color: Beige; -fx-text-fill: Black ; -fx-cursor : hand ;");
        location.setFont(Font.font("Verdana", FontWeight.BOLD, 20));
        location.setOnAction(event -> {
            SearchLocation.showLocationSearch();
        });

        // Author: Yashodip Thakare
        // Desc: Creating a Button object for the "Logout" button
        Button logout = new Button();
        logout.setText("Logout");
        logout.setPrefSize(150, 55);
        logout.setStyle("-fx-background-color: Beige; -fx-text-fill: Black ; -fx-cursor : hand");
        logout.setFont(Font.font("Verdana", FontWeight.BOLD, 20));
        logout.setOnAction(event -> { // Lambda Function override the functional interface consist only one Abstract
                                      // class
            loginPage.handleLogout();
            logout.setEffect(ds);
        });

        // Author: Yashodip Thakare
        // Desc: Creating an HBox object to hold the location and logout buttons
        HBox headRightBox = new HBox(location, logout);
        headRightBox.setSpacing(20);
        headRightBox.setAlignment(Pos.CENTER);

        // Author: Yashodip Thakare
        // Desc: Creating an HBox object for the top header
        HBox TopHeader = new HBox(300, headbuttons, headRightBox);
        TopHeader.setPrefSize(1400, 100);
        TopHeader.setStyle("-fx-background-color : Beige");
        TopHeader.setAlignment(Pos.CENTER);

        // Author: Yashodip Thakare
        // Desc: Creating an HBox object to hold the grid
        HBox hb = new HBox(grid);
        hb.setLayoutY(100);
        hb.setPrefHeight(700);
        hb.setPrefWidth(1400);

        // Author: Yashodip Thakare
        // Desc: Creating a ScrollPane object to hold the HBox
        ScrollPane sc = new ScrollPane(hb);
        sc.setVbarPolicy(ScrollPane.ScrollBarPolicy.ALWAYS);
        sc.setPrefSize(1400, 700);
        sc.setLayoutY(100);

        // Author: Yashodip Thakare
        // Desc: Creating a Pane object to hold the top header and scroll pane
        Pane pane = new Pane();
        pane.getChildren().addAll(TopHeader, sc);

        // Author: Yashodip Thakare
        // Desc: Creating a Scene object to hold the pane
        scene = new Scene(pane, 1400, 800);
        return scene;
    }

    // Author: Yashodip Thakare
    // Desc: A method to show the Dash3 scene
    public static void showDash3Scene() {
        prStage.setScene(scene);
        prStage.getIcons().add(new Image("com/example/Assets/logo2.png"));
        prStage.show();
    }

    // Author: Yashodip Thakare
    // Desc: A method to handle car information
    public void handleCarInfo() {
        // Create a new carInfo object
        carInfo controllCarInfo = new carInfo(dashBoard, dash2, this);
        Scene carinfoScene = controllCarInfo.callCarInfo(prStage);
        prStage.setScene(carinfoScene);
        prStage.show();
    }
}
