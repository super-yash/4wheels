package com.example.Module3.DashBoards;

import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

/**
 * Author: Vaibhav Patil
 * Description: This class provides a GUI for searching nearby cities and displaying the current location.
 */
public class SearchLocation {
    /**
     * The TextArea where the search results will be displayed.
     */
    private static TextArea finalArea;
    private static final String API_URL = "https://wft-geo-db.p.rapidapi.com/v1/geo/cities";
    private static final String API_HOST = "wft-geo-db.p.rapidapi.com";
    private static final String API_KEY = "c6019cf8a0mshab642d0e161fb83p1c33e0jsn83edb843712d";
    
    public static void showLocationSearch() {
        Stage locationStage = new Stage();
        locationStage.setTitle("Search Nearby Cities");

        Label citySearchLabel = new Label("City Location Search:");
       
        TextField citySearchField = new TextField();
        citySearchField.setPromptText("Enter City Name...");
      
        Button searchButton = new Button("Search");
        
         Button locationButton = new Button("Show Current Location");
        locationButton.setOnAction(e -> showLocation());
        
         HBox buttonBox = new HBox(10, searchButton, locationButton);
        buttonBox.setPadding(new Insets(10, 0, 10, 0));

        finalArea = new TextArea();
        finalArea.setEditable(false);

        searchButton.setOnAction(e -> {
            String cityPrefix = citySearchField.getText().trim();
            if (!cityPrefix.isEmpty()) {
                searchCities(cityPrefix);
            }
        });

        VBox vbox = new VBox(10, citySearchLabel, citySearchField, buttonBox, finalArea);
        vbox.setPadding(new Insets(10));
        vbox.setStyle("-fx-font-family: Arial; -fx-font-size: 14px;");

        Scene scene = new Scene(vbox, 500, 300);
        scene.setFill(Color.BEIGE);
         locationStage.setResizable(false);

        locationStage.setScene(scene);
        locationStage.show();
    }

    /**
     * Author: Vaibhav Patil
     * Description: This method searches for cities based on the given prefix.
     * 
     * @param cityPrefix The prefix of the city name to search for.
     */
    private static void searchCities(String cityPrefix) {
        try {
            String query = String.format("?namePrefix=%s", URLEncoder.encode(cityPrefix, StandardCharsets.UTF_8.name()));
            URL url = new URL(API_URL + query);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setRequestProperty("X-RapidAPI-Host", API_HOST);
            connection.setRequestProperty("X-RapidAPI-Key", API_KEY);

            int responseCode = connection.getResponseCode();
            if (responseCode == HttpURLConnection.HTTP_OK) {
                BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                StringBuilder response = new StringBuilder();
                String inputLine;

                while ((inputLine = in.readLine())!= null) {
                    response.append(inputLine);
                }
                in.close();

                JSONObject jsonResponse = new JSONObject(response.toString());
                JSONArray dataArray = jsonResponse.getJSONArray("data");

                StringBuilder result = new StringBuilder();
                for (int i = 0; i < dataArray.length(); i++) {
                    JSONObject cityObject = dataArray.getJSONObject(i);
                    String city = cityObject.getString("city");
                    String country = cityObject.getString("country");
                    String region = cityObject.getString("region");
                    result.append(String.format("City: %s, Country: %s, Region: %s%n", city, country, region));
                }

                finalArea.setText(result.toString());
            } else {
                finalArea.setText("GET request failed. Response Code: " + responseCode);
            }
        } catch (Exception e) {
            finalArea.setText("Error fetching data: " + e.getMessage());
        }
    }

    /**
     * Author: Vaibhav Patil
     * Description: This method displays the current location.
     */
    private static void showLocation() {
        String dummyLocation = "Latitude: 18.5204, Longitude: 73.8567 (Pune, India)";
        finalArea.setText(dummyLocation);
    }
}
