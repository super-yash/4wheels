package com.example.Module3.Module4;

import com.example.Module3.DashBoards.DashBoard;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;

public class SCORPIO {

    private DashBoard dashBoard;
    private Stage prStage ;
    private static Scene scene;

    public SCORPIO(DashBoard dashBoard){
        this.dashBoard = dashBoard ;
    }

   
    public Scene callSCORPIO(Stage prStage) {
       
        this.prStage = prStage ;

        // Car Image
        Image carImage = new Image("com/example/Assets/SCORPIO.jpeg"); // Replace with the correct path to the car image
        ImageView imageView = new ImageView(carImage);
        imageView.setFitWidth(700);
        imageView.setFitHeight(400);
        //imageView.setPreserveRatio(true);

 // Car Title
TextField carTitle = createTransparentTextField("Mahindra Scorpio", 30, true);

// Price
TextField priceLabel = createTransparentTextField("₹ 13.62 - 17.42 Lakh*", 25, true);

// Reviews
TextField reviewsLabel = createTransparentTextField("737 Reviews", 25, false);

// Specifications
TextField specsLabel = createTransparentTextField("Mahindra Scorpio specs & features", 25, true);

// Specifications Details
TextField engineLabel = createTransparentTextField("Engine: 2184 cc", 20, false);
TextField torqueLabel = createTransparentTextField("Torque: 300 Nm", 20, false);
TextField powerLabel = createTransparentTextField("Power: 130 bhp", 20, false);
TextField seatingLabel = createTransparentTextField("Seating Capacity: 7, 9", 20, false);
TextField driveTypeLabel = createTransparentTextField("Drive Type: RWD", 20, false);
TextField fuelLabel = createTransparentTextField("Fuel: Diesel", 20, false);

        VBox specsBox = new VBox(5, engineLabel, torqueLabel, powerLabel,seatingLabel,driveTypeLabel, fuelLabel);
        specsBox.setPadding(new Insets(10));

        // Button
        Button viewOffersButton = new Button("View Complete Offers");
        viewOffersButton.setFont(Font.font("Arial", FontWeight.BOLD, 25));
        viewOffersButton.setStyle("-fx-background-color: #FF6F00; -fx-text-fill: white;");

        // Left Side (Image and Specs)
        VBox leftSide = new VBox(30, imageView);
        leftSide.setPadding(new Insets(20));
        leftSide.setAlignment(Pos.TOP_CENTER);

        // Right Side (Title, Price, Reviews, Button)
        VBox rightSide = new VBox(20, carTitle, priceLabel, reviewsLabel, viewOffersButton);
        rightSide.setPadding(new Insets(20));
        rightSide.setAlignment(Pos.TOP_CENTER);

        VBox bottomsidd = new VBox(specsLabel, specsBox);
        bottomsidd.setAlignment(Pos.TOP_CENTER);
        bottomsidd.setLayoutY(450);
        bottomsidd.setLayoutX(200);
        bottomsidd.setPrefSize(400, 200);

        // Main Layout
        HBox mainLayout = new HBox(50, leftSide, rightSide);
        mainLayout.setAlignment(Pos.CENTER);

        Button Back = new Button();
        Back.setText("Back");
        Back.setFont(Font.font("Arial", FontWeight.BOLD, 20));
        Back.setStyle("-fx-background-color: coral; -fx-cursor : hand; -fx-text-fill: white;");
        Back.setPrefSize(100, 50);
        Back.setLayoutX(20);
        Back.setLayoutY(700);
        Back.setOnAction(event ->{
            DashBoard.showDashBoardscene();
        });

        Pane pane = new Pane(mainLayout,bottomsidd,Back);

        return scene = new Scene(pane, 1400, 800);
       
    }

    private TextField createTransparentTextField(String text, int fontSize, boolean bold) {
        TextField textField = new TextField(text);
        textField.setFont(Font.font("Arial", bold ? FontWeight.BOLD : FontWeight.NORMAL, fontSize));
        textField.setStyle("-fx-background-color: transparent; -fx-border-color: transparent; -fx-text-fill: black;");
        textField.setEditable(false);
        return textField;
    }

}
