 package com.example.Module2.StartPage;

import com.example.Module2.Controller.landingPage1;
import javafx.application.Application;
import javafx.scene.image.Image;
import javafx.stage.Stage;
public class startPageApp extends Application {
    @Override
    public void start(Stage prStage) throws Exception {
        // loginPage loginPage = new loginPage(prStage);
        //prStage.setScene(loginPage.getLoginScene())     
        
        landingPage1 landingPage1 = new landingPage1(prStage);
        prStage.setScene(landingPage1.InitLandingPage1());
        prStage.getIcons().add(new Image("com/example/Assets/logo2.png"));
        prStage.setTitle("4Wheels");
        prStage.setResizable(false);
        prStage.show();
    }
}


